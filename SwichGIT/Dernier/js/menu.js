var FormeSelect = ["rond"];
var ListeCouleur = ["colorbase","colorcarre","colorone","colortwo"];
var ListeCouleurModif = ["colorseven","coloreight","colornine","colorten","colorthree","colorfour","colorfive","colorsix"];
var ListeClassForme = ["rond","carre","triangle","croix","losange","penta","hexa","hocto"];

//3 modes : 0 = couleurs // 1 = formes // 2 = personnaliser les cartes
var mode = 0;
var valRange;

function ableCache(formName){
	cacheName = "cache"+formName;
	const pos = FormeSelect.indexOf(formName);
	if (pos > -1) {
		FormeSelect.splice(pos, 1);
	}
	document.getElementById(cacheName).style.display = "block";
}

function disableCache(formName){
	if (FormeSelect.length < 4)
	{
		cacheName = "cache"+formName;
		document.getElementById(cacheName).style.display = "none";
		FormeSelect.push(formName);
		for(const form in ListeClassForme){
			//si la couleur est dans la liste selectionnée 
			if(formName == ListeClassForme[form]){
				//recuperer la position de la couleur dans laquel on va le mettre		
				var positionColor = FormeSelect.length - 1;
				var recupCouleur = getComputedStyle(document.body).getPropertyValue('--'+ListeCouleurModif[form]);
				var dansQuelCouleur = '--'+ListeCouleur[positionColor];

				document.documentElement.style.setProperty(dansQuelCouleur, recupCouleur);

				console.log("Couleur "+ recupCouleur +" mettre dans : " + dansQuelCouleur);
			}
		}
	}
}

function changeContour(value){
	var newvalue = value * 0.055 + 1;
	document.documentElement.style.setProperty('--z', newvalue+'vw');
	var valcard = value * 0.013 + 1;
	document.documentElement.style.setProperty('--n', valcard+'vw');
	//document.documentElement.style.setProperty('--n', newvalue+'vw');
	//document.getElementById("SettingName").innerHTML = newvalue;
}

function getVarColor(color){
	var getvar = color;
	getvar = getvar.substring(6,getvar.length-1);

	return getvar;
}

function varColorToHex(color){
	hex = getComputedStyle(document.documentElement).getPropertyValue(color);
	hex = hex.substring(1,hex.length);
	return hex;
}

function settingOpen(){
	//document.getElementById("iddelapartie").style.visibility = "hidden";
	var elements = document.getElementsByClassName("pcr-button");
	for (var i = 0; i < elements.length; i++) {
		elements[i].classList.add(ListeClassForme[i]);
	}
	recupCookies();
}

function SettingClose(){
	//save cookies
	document.cookie = "forme1="+FormeSelect[1]+";secure";
	document.cookie = "forme2="+FormeSelect[2]+";secure";
	document.cookie = "forme3="+FormeSelect[3]+";secure";

}

function recupCookies(){
	var theCookies = document.cookie.split(';');
	theCookies[0] = ' '+theCookies[0];
	var FormeCookie = [];

	for(var i = 1; i <= theCookies.length; i++){
		var aString = theCookies[i-1].substring(8);
		if(aString != "undefined"){
			FormeCookie.push(aString);
		}
	}

	for(const form in FormeCookie)
	{
		if(FormeSelect.indexOf(FormeCookie[form]) < 0){
			disableCache(FormeCookie[form]);
		}
	}

	console.log(FormeCookie);
	console.log("Formes select : "+FormeSelect);

}

function affichageParamMulti(num){
	//si 1 affichage, si 0 on cachell
	if(num == 1){
		document.getElementById("multiParams").style.display = "block";
	} else {
		document.getElementById("multiParams").style.display = "none";
	}
}

