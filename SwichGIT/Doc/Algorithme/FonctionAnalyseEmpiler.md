# ANALYSE DE CARTE #

## DEFINITION DES STRUCTURES ##
### ENUMERATION FORMEFIGURE ###
//formes géométriques possibles sur des cartes

- **Rond** :rond
- **Carre** :carre
- **Triangle** : triangle
- **Pentagone** : pentagone
- **Losange** : losange

---
### ENUMERATION TYPEFIGURE ###
//Tailles de Figure possibles sur des cartes

- **Petit** : petit
- **Moyen** : moyen
- **Grand** : grand

---
### STRUCTURE FIGURE ###
### Attributs ###
- **Figure.type** : TypeFigure [ ]
- **Figure.forme** : FormeFigure
- **Figure.X** : entier // Coordonnées X sur la **Carte**
- **Figure.Y** : entier // Coordonnées Y sur la **Carte**
- **Figure.HTML** : code HTML
### Méthodes ###
- **Figure.get...**: Retourne l'attributs voulus

---
###STRUCTURE CARTE ###

### Attributs ###
- **Carte.identifiant** : entier
-  **Carte.row** : eniter // Nombre de lignes sur la carte
-  **Carte.column** : entier // Nombre de colonnes sur la carte
- **Carte.HTML**: code HTML
- **Carte.Matrice**: entier[row][column];
- **Carte.lesFigures** : Figures [ ]
### Méthodes ###
- **Carte.carteVerticale** : obtient sa carte verticalement symétrique.
- **Carte.carteHorizontale** : obtient sa carte horizontalement symétrique.
- **Carte.get...** : Retourne l'attributs voulus

---
### STRUCTURE CODE ###
// Permet de gérer les informations sur des assemblages de cartes

### Attributs ###
- **Code.code** : entier //Nombre de liaisons
- **Code.Carte**: Carte

---

## COMPARAISON DE CARTE ##

**ComparaisonDeCarte(CarteMere: Carte,  CarteFille: Carte)** : entier,Carte

**Entrées** : 

- **CarteMere** : Carte témoin, "support" de l'assemblage.

-  **CarteFille** : Carte sujette, "pièce" à comparer sur le support.

**Objectif** : Determine le nombre de liaison possible entre les deux cartes envoyées en paramètre

**Sortie** : Code

**Sortie Possible :**

- new Code(**-1,CarteMere**) : L'assemblage est impossible.

- new Code(**nbliaisons,CarteMere**) : L'assemblage est possible avec un certain nombre de liaisons entre les deux cartes.


### COMMENT FAIRE ###
- Pour chaque **Figure** contenu dans le second paramètre de la fonction (**CarteFille.lesFigures**) :
	- On regarde chaque **Figure** contenu dans le premier parametre (**CarteMere.lesFigures**) puis on vérifie ces situations :
	- Si deux figures sont aux mêmes endroits, de même **forme** et de **type** différents. Dans ce cas, on incrémente le nombre de liaisons.
	- Si des figures sont aux mêmes endroits mais que les **Types** sont identiques ou que les **Forme** sont différentes. Dans ce cas, il est impossible d'assembler les deux **Cartes**, le nombre de liaisons vaut alors -1 et on retourne le résultat.
- A la fin de la comparaison des **Figures**. On fait la mise à jour du premier paramètre (**CarteMere**) en faisant la fusion des deux **Cartes** puis on retourne le résultat avec le nombre de liaison dans une nouvelle structure **Code**.

---


## SOMME DE CARTE #

**SommeDeCarte(CarteMere:Carte d'entiers,CarteFille:Carte)**:Carte

**Entrees** :

 - **CarteMere** : Carte témoin, Carte mise à jour
-  **CarteFille** : Carte sujette,Carte comportant les nouveaux ajouts

**Objectif** : Mettre à jour **CarteMere**  en ajoutant les propriétés de **CarteFille**

**Sortie** : Carte

### COMMENT FAIRE ###
- Pour chaque **Figure** contenu dans **CarteFille.lesFigures**
	- On regarde chaque **Figure** contenu dans **CarteMere.lesFigures**
	- Si l'une des Figures de **CarteFille** est absente de **CarteMere** on l'ajoute à la liste **CarteMere.lesFigures**.
	- Sinon on fait rien
- Une fois toutes les **figures** mises à jour dans **CarteMere.lesFigures**, **CarteMere.Matrice** est aussi mise à jour par le biais de somme de puissance de 2 en fonction des **types** de **Figures**.

---

Dans une Matrice si nous avons une somme de puissance de 2 dans une cellule, nous sommes capable de déterminer quels types de figures sont présent dans celle-ci : 

- 1 -> Petit
- 2 -> Moyen
- 3 -> Petit Moyen
- 4 -> Grand
- 5 -> Gand Petit
- 6 -> Grand Moyen
- 7 -> Grand Moyen Petit

---
- On retourne **CarteMere** mise à jour
> ---

# ASSEMBLAGE #

**Assemblage(TasDeCarte: Carte[], CarteMere:Carte** : Code[ ]

**Entree** : 

 - **TasDeCarte** : Liste de **Carte** selectioonnées afin de tester si un assemblage valide est possible.
 -  **CarteMere** : Carte témoin, "support" de l'assemblage.

**Objectif** : Tester tous les assemblages possibles du tas de cartes

**Sortie** : Code[ ]

### COMMENT FAIRE ###

- On supprime la tête de liste envoyée en paramètre qui, au début de cette fonction, est identique au second paramètre de la fonction (**CarteMere**).
- Si cette liste, après la suppression de sa tête, est maintenant vide, on retourne une structure **Code** avec pour attribut **Code.code** = 0 et **Code.Carte** = **CarteMere**.
	-Sinon on continue la fonction  
- On appelle récursivement cette fonction (**Assemblage**) en envoyant en premier paramètre la nouvelle liste de **Carte**, et en second parametre, toutes les variantes (rotations) possible de la **Carte** en tête de cette même liste.
- Une fois tout les résultats de ces fonctions récupérés dans une seule liste:
	- Pour chaque index de cette liste :
		- Si nous sommes notifiés via la structure **Code** présente à cet index que l'on peut assembler les cartes, alors on compare la **Carte** et **CarteMere**. Puis on ajoute le nombre de liaison entre elles.
- On retourne la liste de résultats mise à jour.

---
















