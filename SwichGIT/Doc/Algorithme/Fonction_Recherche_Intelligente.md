# RECHERCHE DE SWISH INTELLIGENTE#

Ce document, présente une version de l'algorithme de recherche utilisé dans notre projet. Il a pour objectif d'énumérer tous les Swish possibles de 2, 3, 4, ou 5 cartes sur un plateau de jeu présentées à un joueur. Il pourra également être utilisé pour aider les joueurs si aucun Swish n'est possible et ainsi débloquer le jeu ou arrêter la partie.

## DEFINITION DES STRUCTURES ##

### ENSEMBLE ###
### Attributs ###
//Permet de déterminer si un ensemble de carte peut **théoriquement** former un Swish en regardant uniquement la couleur de leurs **Figures**

- **ensemble** //Liste d'entier représentant les index des cartes dans le plateau de jeu
- **v** //Entier notifiant si l'ensemble d'index peut **théoriquement** former un Swish

---

## DEFINITION DES FONCTIONS UTILISEE POUR CETTE RECHERCHE ##

# SOMME CARRE ELEMENT #

**Objectif** : Calculer la somme des puissances de 2 d'une liste d'entier envoyé en paramètre.

**sommeCarreElement(liste: int[])** : Int

**Entrées** :

	-**liste** : liste d'entier

**Sortie** : 

	-**int** : Somme des puissances de 2 de l'ensemble

### COMMENT FAIRE ###

- Pour chaque entier présent dans la liste envoyé en parametre

	- Mettre 2 à la puissance de l'entier présent à cet index et faire une somme.

Une fois la somme calculée, on retourne le résultat.

---

# COMPARAISON EMPILEMENT #

**Objectif** : Enlever les doublons d'une liste envoyée en paramètre.

**Pourquoi cette fonction ?** : Notre recherche intelligente est capable de trouver tout les SWISH possibles sur un plateau de jeu mais également TOUTES les façons de les obtenir. Pour des SWISH à 4 ou 5 cartes, le temps de recherche est fortement allongé du fait que pour un SWISH avec un tas composé de n **Cartes"**, il existe 4^n variantes de ce même tas.

Calculer plusieurs fois le même tas différements mélangé fais perdre beaucoup de temps. Cette fonction va donc, en calculant ces sommes, enlever les doublons et gagner du temps pour le confort de l'utilisateur.

**comparaisonEmpilement(liste: Ensemble[], debut: int)** : int[]

**Entrées** :

	-liste** : Liste d'Ensemble, contient tout les ensembles qui peuvent **théoriquement** former un Swish.

	- debut** : entier permettant de savoir à quel index ded la liste nous sommes

**Sortie** : 

	-**Ensemble[]** : Nouvelle liste d'ensemble ne contenant plus de doublon **d'Ensemble**

### COMMENT FAIRE ###

-Si début est supérieur ou égal à l'index du dernier élément de la liste cela signifie que l'algorithme possède une liste sans doublon et a terminé ses opérations. On retourne donc la liste actuelle.

-On récupère tout les éléments de la liste de 0 jusqu'à l'index **début** qui ne sont pas en doublon.

-On calcule la **sommeCarreElement** de l'élément présent à l'index **début** de la liste pour l'identifier.

-Une fois cette somme récupérée, on la compare avec la **sommeCarreElement** de tous les éléments de la liste restant 

-Si les **sommeCarreElement** sont différent on récupère l'élément à cet index
-Une fois les doublons de l'élément à l'index **début** retiré, on appelle récursivement cette fonction **comparaisonEmpilement()** avec pour paramètre la liste mis à jour et début incrémenté.

---

# RECHERCHE CARTE COMPLEMENTAIRE #

-**Objectif**: Rechercher toutes les cartes du plateau de jeu contenant une figure complémentaire (type différent et couleur identique) avec la figure envoyée en paramètre.


**carteComplementaire**(figure: Figure, debut: int): int[]

**Entrées** :

	-**figure**: Structure **Figure** depuis laquelle nous allons rechercher toutes les cartes avec une **Figure** complémentaire.

	-**debut** : Entier permettant de notifier où l'on se trouve dans le plateau de jeu dans le but de ne pas créer de boucle dans les recherches.

**Sortie**

	-**int[]** : Liste d'entier contenant les index des cartes comportant une **Figure** complémentaire dans le plateau de jeu.

### COMMENT FAIRE ###

-Pour toutes les cartes du plateau de jeu située entre l'index début et la fin du plateau.

	-On regarde si les cartes ont une **Figure** de type différent et de couleur identique au parametre **figure** envoyée dans la fonction.
		-Si c'est le cas un ajoute l'index de cette carte dans une liste.
		-Sinon rien.

-Une fois toutes les cartes du plateau de jeu vérifiées, on retourne la liste d'index remplie

---

# RECHERCHE ENSEMBLE INTELLIGENT #

-**Objectif**: Retourner une liste **d'Ensemble** contenant des ensembles de carte qui peuvent former un Swish par rapport à un indice de carte donné.

**rechercherPertinent**(nbCarte: int, debut: int, carte: int): Ensemble[]

**Entrées**:

	-**nbCarte**: Information sur le nombre de **Cartes** que l'algorithme doit encore trouver pour chercher un SWISH à n **Cartes**

	-**debut** : Indice dans le plateau de jeu du support du SWISH (première carte de l'empilement)

	-**carte** : Indice dans le plateau de jeu de la carte que l'algorithme traite actuellement.

**Sortie**

	-**Ensemble[]** : Liste **d'Emssemble** contenant de liste d'indice pouvant potentiellement former des SWISH

### COMMENT FAIRE ###

-Si l'algorithme n'a plus de carte à rechercher (**nbCarte** = 0)

	-On regarde si les figures de la première et de la dernière carte de l'ensemble sont complémentaire

		- Si elles le sont, on ajoute l'indice de la carte que l'algorithme traite (**carte**) dans une structure **Ensemble** et on la retourne

		- Si elles ne le sont pas, on ajoute l'indice de la carte que l'algorithme traite (**carte**) dans une structure **Ensemble** et on la retourne en notifiant **l'Enssemble** que cette liste est mauvaise.

-On recupère tout les index des **Cartes** pouvant compléter le point de la **Carte** à l'index **carte**

-Si la liste d'indice est vide alors on retourne un code d'erreur

-Sinon pour chaque index de carte complémentaire on appelle récursivement cette fonction **recherchePertinente** en envoyant, nbCarte décrémenté, debut et l'indice de la carte.

-Une fois tous ces résultats récupérés on les retournes.

---


# TEST DES ENSEMBLES #

-**Objectif**: Tester tous les **Ensemble** d'une liste notifier et compter si les **Ensemble** sont correcte 

**testAllEnsemble**(liste: Ensemble[]):int

**Entrees** :

	-**liste**: Liste **d'Ensemble** à tester

**Sortie** :

	-**int**: Correspond au nombre de Swish trouvé

### COMMENT FAIRE ###

-Pour toutes les **Ensembles** présents dans la liste

	-On récupère les cartes correspondant aux indices

		-Une fois toutes les cartes récupérées on teste notre tas dans la fonction **AssemblageARBRE**

		-Si le SWISH est possible on incrémente le nombre de SWISH trouvé et on recommence avec le tas de carte suivant

-Une fois tous les **Ensembles** on retourne le nombre de SWISH trouvé.

---

# TEST INTELLIGENT #


-**Objectif**: Rechercher tous les SWISH possibles dans un plateau de jeu.

-**testPertinent**(void):void

### COMMENT FAIRE ###

-Pour des tas allant de 2 à 5 **Carte**

	-Pour chaque **Carte** du plateau de jeu

			-On lance une **recherchePertinente** en envoyant le nombre de **Cartes** de notre tas, le numéro de la carte à partir de laquelle on veut les **Ensembles**possibles.

	-Une fois tous les **Ensembles** récupérés on vérifie qu'il n'y a pas de doublon en lançant la fonction **comparaisonElement** de ces résultats.

	-Une fois les doublons retirés on teste tous les **Ensembles** dans la fonction **testAllEnsemble**
	-On affiche le nombre de SWISH trouvé et on fait apparaitre sur l'écran du joueur un SWISH à 5 cartes.

---
