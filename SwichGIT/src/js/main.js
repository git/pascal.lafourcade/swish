var mode = 1;
var nbplayers = 1;
var lange = 0;
var lightprima = "#DEDEDE";
var lightsedonca = "#B7B7B7";
var lighttercia = "#E6E6E6";

var darkprima = "#474747";
var darkseconda = "#393939";
var darktercia = "#606060";
var carteselect = [];
var deckPartie = []; //Ensemble des cartes affichees a l'ecran
var TasDuJEU = []; // Ensemble de toutes les cartes presente dans le jeu
var allDimension = ["4 * 3", "3 * 3"];// Défini toutes les dimensions que les cartes peuvent avoir
var selectionMode = 0;
var selectionDiemension = 0;
var allModeFrench = ["Match à Point", "Mode Timer", "Infini", "MultiJoueur"]; // Défini tout les modes de Jeu en français
var allModeEngl = ["Point Match", "Timer mode", "Infinity", "MultiPlayer"]; // Défini tout les modes de Jeu en Anglais
var tab2=[];
var modelangue = false;
var offreSWISH = false;
var carteselectPos = [];

var lesPoints = 0; //Points du Joueur1

function majSizeCard(column) {
	var newTailleOrdi = column * 3.3 + 1;
	var newTailleTablette = newTailleOrdi
	document.documentElement.style.setProperty('--taille', newTailleOrdi + "vw");
	document.documentElement.style.setProperty('--tailleTablette', newTailleTablette + "vw");
}
function lancerGameMulti() {
	creePartieClassique3_4();
	document.getElementById("pageAccueil").style.visibility = "hidden";
	//document.getElementById("PageLobbyMulti").style.visibility = "hidden";
	document.getElementById("pageGame").style.visibility = "visible";
	document.getElementById("idGameMulti").style.visibility = "visible";
	document.getElementById("progressbar").style.visibility = "hidden";
	//Suprimer les paarties précédantes
	/*
	$(document).ready(function () {
		//alert("Clean bases")
		$("#div1").load('cleanBase.php')
	});
   */

	//Créer les 16 lignes dans la base pour les 16 cartes de la partie
	//Génère l'idPartie

	//Envoyer les 16 cartes
	listecartes=[];
	for (var i = 0; i < deckPartie.length; i++) {
		var ajt = deckPartie[i].getMatrice[0] + deckPartie[i].getMatrice[1]  + deckPartie[i].getMatrice[2];
		listecartes.push(ajt);
		//console.log(ajt);
	}


	$(document).ready(function () {
		var cartestables = listecartes;

		$.ajax({
			data: {
				cartestables: cartestables
			},
			type: "post",
			url: "envoyercartes.php",
			success: function (data) {
				console.log("====Les cartes ont été envoyés (lancerpartie)===")
			}
		});
	});
}
function ConvertInttoCarte(ListeDesCartes){
	//console.log("LC",ListeDesCartes);
	if(ListeDesCartes.length==0){
		console.log("Vide");
	}
	var lescartes=[]; //Liste des cartes
	for(let j =0 ; j<ListeDesCartes.length;j++){
		var lesfigure=[]; //Liste figures de la carte

		//pb?
		for (let i = 0; i < ListeDesCartes[j].length; i++) {

			if(ListeDesCartes[j][i]==1){
				var premierefigure= new Figure(TypeFigure.Petit,FormeFigure.Rond,Math.floor(i/4),(i%4));
				lesfigure.push(premierefigure);
				//console.log("cc1",j," ",i);
			}
			else if(ListeDesCartes[j][i]==2){
				var deuxiemefigure = new Figure(TypeFigure.Moyen,FormeFigure.Rond,Math.floor(i/4),(i%4));
				lesfigure.push(deuxiemefigure);
				//console.log("cc2",j," ",i);
			}
		}
		//pb?

		var carte= new Carte(lescartes.length,lesfigure,4,3);
		//console.log("MAMAN2",j," ",carte);
		lescartes.push(carte);
	}
	//console.log("LC2",lescartes);
	deckPartie = lescartes;
	afficherCartes(deckPartie);
	console.log("--Conversion réussie--");
	// console.log("CARTES SELECTS : ",carteselectPos);
	reselectCartes();
}
function reselectCartes() {
	if (carteselectPos != [] && carteselectPos!=null) {
		// console.log("cartesPOS: ",carteselectPos);
		carteselectPos.forEach((pos) => {
			var lacarte = "card"+pos;
			var numeroCarte = pos;
			//console.log("lacarte: ",lacarte);
			//console.log(deckPartie[numeroCarte].get);
			document.getElementById(lacarte).style.boxShadow = "0 0 1vw red, 0 0 1vw red";
		});
	}
	else {
		for (var i=0; i<deckPartie.length;i++) {
			var lacarte = "card"+i;
			var numeroCarte = i;
			//console.log("lacarte: ",lacarte);
			//console.log(deckPartie[numeroCarte].get);
			document.getElementById(lacarte).style.boxShadow = "";
		}
	}
}
function rejoindreGameMulti() {
	creePartieClassique3_4();
	document.getElementById("pageAccueil").style.visibility = "hidden";
	//document.getElementById("PageLobbyMulti").style.visibility = "hidden";
	document.getElementById("pageGame").style.visibility = "visible";
	document.getElementById("idGameMulti").style.visibility = "visible";
	document.getElementById("progressbar").style.visibility = "hidden";
}
function modePrec() {
	if(!modelangue){
		if (selectionMode == 0) {
			selectionMode = allModeFrench.length - 1;
		}
		else {
			selectionMode -= 1;
		}
		document.getElementById("textmode").textContent = "" + allModeFrench[selectionMode]
		if(allModeEngl[selectionMode] == "MultiPlayer" || allModeEngl[selectionMode] == "MultiJoueur"){
			affichageParamMulti(1);
		} else {
			affichageParamMulti(0);
		}
	}
	else{
		if (selectionMode == 0) {
			selectionMode = allModeEngl.length - 1;
		}
		else {
			selectionMode -= 1;
		}
		document.getElementById("textmode").textContent = "" + allModeEngl[selectionMode]
		if(allModeEngl[selectionMode] == "MultiJoueur" || allModeEngl[selectionMode] == "MultiPlayer" ){
			affichageParamMulti(1);
		} else {
			affichageParamMulti(0);
		}
	}
	var table1 = document.getElementById('table_HS_Timer');
	var table2 = document.getElementById('table_HS_Match');

	if(selectionMode == 0) {
		table1.style.display = "none";
		table2.style.display = "block";
	}
	if(selectionMode == 1) {
		table1.style.display = "block";
		table2.style.display = "none";
	}
	if(selectionMode == 2) {
		table1.style.display = "none";
		table2.style.display = "none";
	}
	if(selectionMode == 3) {
		table1.style.display = "none";
		table2.style.display = "none";
	}


}



function dimensionPrec() {
	if (selectionDiemension == 0) {
		selectionDiemension = allDimension.length - 1;
	} else {
		selectionDiemension -= 1;
	}
	document.getElementById("textdimension").textContent = "" + allDimension[selectionDiemension]

	if(!modelangue){
		if (selectionMode == 0) {
			selectionMode = allModeFrench.length - 1;
		} else {
			selectionMode -= 1;
		}
		document.getElementById("textmode").textContent = "" + allModeFrench[selectionMode]
	}
	else{
		if (selectionMode == 0) {
			selectionMode = allModeEngl.length - 1;
		} else {
			selectionMode -= 1;
		}
		document.getElementById("textmode").textContent = "" + allModeEngl[selectionMode]
	}

}

function modeSuiv() {
	if(!modelangue){
		if (selectionMode == allModeFrench.length - 1) {
			selectionMode = 0;
		} else {
			selectionMode += 1;
		}
		document.getElementById("textmode").textContent = "" + allModeFrench[selectionMode]
		if(allModeEngl[selectionMode] == "MultiPlayer" || allModeEngl[selectionMode] == "MultiJoueur"){
			affichageParamMulti(1);
		} else {
			affichageParamMulti(0);
		}
	}
	else{
		if (selectionMode == allModeEngl.length - 1) {
			selectionMode = 0;
		} else {
			selectionMode += 1;
		}
		document.getElementById("textmode").textContent = "" + allModeEngl[selectionMode]
		if(allModeEngl[selectionMode] == "MultiJoueur" || allModeEngl[selectionMode] == "MultiPlayer"){
			affichageParamMulti(1);
		} else {
			affichageParamMulti(0);
		}
	}
	var table1 = document.getElementById('table_HS_Timer');
	var table2 = document.getElementById('table_HS_Match');

	if(selectionMode == 0) {
		table1.style.display = "none";
		table2.style.display = "block";
	}
	if(selectionMode == 1) {
		table1.style.display = "block";
		table2.style.display = "none";
	}
	if(selectionMode == 2) {
		table1.style.display = "none";
		table2.style.display = "none";
	}
	if(selectionMode == 3) {
		table1.style.display = "none";
		table2.style.display = "none";
	}

}

function dimensionPrec() {
	if (selectionDiemension == 0) {
		selectionDiemension = allDimension.length - 1;
	} else {
		selectionDiemension -= 1;
	}
	document.getElementById("textdimension").textContent = "" + allDimension[selectionDiemension]
}


function dimensionSuiv() {
	if (selectionDiemension == allDimension.length - 1) {
		selectionDiemension = 0;
	} else {
		selectionDiemension += 1;
	}
	document.getElementById("textdimension").textContent = "" + allDimension[selectionDiemension]
}


class Carte {

	constructor(id, allFigure, nbRow, nbColumn/*,nbForme*/) { //a decommenter quand il y aura des prametres de partie
		//Attribut de Classe
		this.identifiant = id;
		this.row = nbRow;
		this.column = nbColumn;
		this.Matrice = creaMatriceVierge(nbRow, nbColumn);
		for (var i = 0; i < allFigure.length; i++) {
			if (allFigure[i].type[0] == TypeFigure.Petit) {
				this.Matrice[allFigure[i].X][allFigure[i].Y] += 1;
			} else {
				this.Matrice[allFigure[i].X][allFigure[i].Y] += 2;
			}
		}

		this.SesFigures = [];
		for (var i = 0; i < allFigure.length; i++) {
			this.SesFigures.push(allFigure[i]);
		}

		//lien clicable
		this.link = document.createElement('a');
		this.link.id = this.identifiant;

		//div carte
		var divconteneur = document.createElement('div');
		divconteneur.className = "flex-item";
		divconteneur.id = "card" + this.identifiant;

		for (var i = 0; i < this.row * this.column; i++) {

			var divcase = document.createElement('div');
			divcase.className = "item-form";
			var ajoute = true;
			for (var j = 0; j < this.SesFigures.length; j++) {
				if (i == this.SesFigures[j].X + (this.SesFigures[j].Y * 3)) {
					try {
						ajoute = false;
						var uneFigure = new Figure(this.SesFigures[j].type, this.SesFigures[j].forme, this.SesFigures[j].X, this.SesFigures[j].Y);
						divconteneur.appendChild(this.SesFigures[j].CodeHTML);
					} catch (e) {
						window.alert(this.SesFigures[j].type.length).length
					}
				}
			}
			if (ajoute) {
				divconteneur.appendChild(divcase);
			}
		}

		this.link.appendChild(divconteneur);
		//fonction de selection
		this.link.onclick = function selectioncarte() {
			var macarte = this.id;
			var lacarte = "card" + macarte;

			for (var i = 0; i < deckPartie.length; i++) {
				if (this.id == deckPartie[i].identifiant) {
					if (carteselectPos.includes(i)) {
						carteselectPos.splice(carteselectPos.indexOf(i), 1);
					} else {
						if(carteselectPos.length<5) {
							carteselectPos.push(i);
						}
					}
				}
			}
			//console.log("cartesPOS: ",carteselectPos);
			var laCarte;
			for (var i = 0; i < deckPartie.length; i++) {
				if (this.id == deckPartie[i].identifiant) {
					laCarte = deckPartie[i];//Carte liee avec le code HTML
				}

			}

			//cas où la carte a déjà été cliqué
			if (document.getElementById(lacarte).style.boxShadow != "") {
				var pos = carteselect.indexOf(laCarte);
				carteselect.splice(pos, 1);
				document.getElementById(lacarte).style.boxShadow = "";
			}

			//cas où la carte n'a pas déjà été cliqué
			else {
				if (carteselect.length < 5) {
					carteselect.push(laCarte);
					document.getElementById(lacarte).style.boxShadow = "0 0 1vw red, 0 0 1vw red";
				}
				else
					window.alert("5 max !");
			};


			// for( var i =0; i < carteselectPos.length;i++){
			// 	carteselect=[];
			// 	//var carte = tab2[1];
			// 	//console.log(carteselectPos[i]);
			// 	//carteselect.push(carte);
			// 	console.log(tab2[carteselectPos[i]]);
			// 	var slt = ConvertUneCarte(tab2[carteselectPos[i]]);
			// 	console.log("C'est moi",slt.getMatrice);
			// }

		};
	}



	get carteVerticale() {

		for (var i = 0; i < this.SesFigures.length; i++) {
			this.SesFigures[i].X = this.column - this.SesFigures[i].X - 1;
		}

		this.Matrice = creaMatriceVierge(this.row, this.column);

		for (var i = 0; i < this.SesFigures.length; i++) {
			for (var j = 0; j < this.SesFigures[i].type.length; j++) {
				if (this.SesFigures[i].type[j] == TypeFigure.Petit) {
					this.Matrice[this.SesFigures[i].X][this.SesFigures[i].Y] += 1;
				} else if (this.SesFigures[i].type[j] == TypeFigure.Moyen) {
					this.Matrice[this.SesFigures[i].X][this.SesFigures[i].Y] += 2;
				} else if (this.SesFigures[i].type[j] == TypeFigure.Grand) {
					this.Matrice[this.SesFigures[i].X][this.SesFigures[i].Y] += 4;
				}
			}
		}
		return this;

	}

	get carteHorizontale() {

		for (var i = 0; i < this.SesFigures.length; i++) {
			this.SesFigures[i].Y = this.row - this.SesFigures[i].Y - 1;
		}

		this.Matrice = creaMatriceVierge(this.row, this.column);

		for (var i = 0; i < this.SesFigures.length; i++) {
			for (var j = 0; j < this.SesFigures[i].type.length; j++) {
				if (this.SesFigures[i].type[j] == TypeFigure.Petit) {
					this.Matrice[this.SesFigures[i].X][this.SesFigures[i].Y] += 1;
				} else if (this.SesFigures[i].type[j] == TypeFigure.Moyen) {
					this.Matrice[this.SesFigures[i].X][this.SesFigures[i].Y] += 2;
				} else if (this.SesFigures[i].type[j] == TypeFigure.Grand) {
					this.Matrice[this.SesFigures[i].X][this.SesFigures[i].Y] += 4;
				}
			}
		}
		return this;
	}

	get getHTML() {
		return this.link;
	}

	get getIdentifiant() {
		return this.identifiant;
	}

	get getMatrice() {
		return this.Matrice;
	}

	get getSesFigures() {
		return this.SesFigures;
	}
}


class Figure {

	constructor(leType, forme, Cox, Coy, couleur) {
		this.X = Cox;
		this.Y = Coy;
		this.type = [];
		for (var i = 0; i < leType.length; i++) {
			this.type.push(leType[i]);
		}
		this.couleur = couleur;
		this.forme = forme;
		if (selectionDiemension == 2 || selectionDiemension == 3) {
			this.pos = this.X + this.Y * 2;
		} else {
			this.pos = this.X + this.Y * 3;
		}
		var divcase = document.createElement('div');
		divcase.className = "item-form";
		//AJOUT FRANCOIS
		//Pour chaque type on regarde les formes
		switch (this.forme) {
			case FormeFigure.Rond:
				switch (this.type[0]) {

					case TypeFigure.Moyen:

						var containInte = document.createElement('div');
						var formeInte = document.createElement('div');
						var containExte = document.createElement('div');
						var formeExte = document.createElement('div');

						var vidcontainer = document.createElement('div');
						vidcontainer.className = "containform3carte";

						formeExte.className = "formeexte";
						formeExte.classList.add('rondv2');

						formeInte.className = "formeinte";
						formeInte.classList.add('rondv2');
						formeInte.style.backgroundColor = "var(--lighttercia)";

						formeExte.style.backgroundColor = "var(--colorbase)";

						if (this.couleur != null) {
							if (this.couleur == CouleurFigure.Couleur1){
								formeExte.style.backgroundColor = "var(--colorbase)"
							} else if (this.couleur == CouleurFigure.Couleur2) {
								formeExte.style.backgroundColor = "var(--colorcarre)";
								if(FormeSelect.length > 1)
								{
									//suppression formeBase
									formeExte.classList.remove('rondv2');
									formeInte.classList.remove('rondv2');
									//ajout forme
									formeExte.classList.add(FormeSelect[1]);
									formeInte.classList.add(FormeSelect[1]);
									//verification triangle ou penda
									if(FormeSelect[1] == "triangle"){
										formeInte.classList.add("castrianglec");
									}
									if(FormeSelect[1] == "penta"){
										formeInte.classList.add("caspentac");
									}
								}
							} else if (this.couleur == CouleurFigure.Couleur3) {
								formeExte.style.backgroundColor = "var(--colorone)";
								if(FormeSelect.length > 2)
								{
									//suppression formeBase
									formeExte.classList.remove('rondv2');
									formeInte.classList.remove('rondv2');
									//ajout forme
									formeExte.classList.add(FormeSelect[2]);
									formeInte.classList.add(FormeSelect[2]);
									//verification triangle ou penda
									if(FormeSelect[2] == "triangle"){
										formeInte.classList.add("castrianglec");
									}
									if(FormeSelect[2] == "penta"){
										formeInte.classList.add("caspentac");
									}
								}
							} else if (this.couleur == CouleurFigure.Couleur4) {
								formeExte.style.backgroundColor = "var(--colortwo)";
								if(FormeSelect.length > 3)
								{
									//suppression formeBase
									formeExte.classList.remove('rondv2');
									formeInte.classList.remove('rondv2');
									//ajout forme
									formeExte.classList.add(FormeSelect[3]);
									formeInte.classList.add(FormeSelect[3]);
									//verification triangle ou penda
									if(FormeSelect[3] == "triangle"){
										formeInte.classList.add("castrianglec");
									}
									if(FormeSelect[3] == "penta"){
										formeInte.classList.add("caspentac");
									}
								} else if (FormeSelect.length > 1){
									//suppression formeBase
									formeExte.classList.remove('rondv2');
									formeInte.classList.remove('rondv2');
									//ajout forme
									formeExte.classList.add(FormeSelect[1]);
									formeInte.classList.add(FormeSelect[1]);
									//verification triangle ou penda
									if(FormeSelect[1] == "triangle"){
										formeInte.classList.add("castrianglec");
									}
									if(FormeSelect[1] == "penta"){
										formeInte.classList.add("caspentac");
									}
								}
							}
						} else {
							this.couleur = CouleurFigure.Couleur1;
							if (selectionDiemension == 0) {
								this.couleur = CouleurFigure.Couleur1;
								if (this.pos == 0 || this.pos == 2 || this.pos == 9 || this.pos == 11) {
									formeExte.style.backgroundColor = "var(--colorcarre)";
									this.couleur = CouleurFigure.Couleur2;
									if(FormeSelect.length > 1)
									{
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[1]);
										formeInte.classList.add(FormeSelect[1]);
										//verification triangle ou penda
										if(FormeSelect[1] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[1] == "penta"){
											formeInte.classList.add("caspentac");
										}
									}
								}
								if (this.pos == 1 || this.pos == 10) {
									formeExte.style.backgroundColor = "var(--colorone)";
									this.couleur = CouleurFigure.Couleur3;
									if(FormeSelect.length > 2)
									{
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[2]);
										formeInte.classList.add(FormeSelect[2]);
										//verification triangle ou penda
										if(FormeSelect[2] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[2] == "penta"){
											formeInte.classList.add("caspentac");
										}
									}
								}
								if (this.pos == 4 || this.pos == 7) {
									formeExte.style.backgroundColor = "var(--colortwo)";
									this.couleur = CouleurFigure.Couleur4;
									if(FormeSelect.length > 3)
									{
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[3]);
										formeInte.classList.add(FormeSelect[3]);
										//verification triangle ou penda
										if(FormeSelect[3] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[3] == "penta"){
											formeInte.classList.add("caspentac");
										}
									} else if (FormeSelect.length > 1){
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[1]);
										formeInte.classList.add(FormeSelect[1]);
										//verification triangle ou penda
										if(FormeSelect[1] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[1] == "penta"){
											formeInte.classList.add("caspentac");
										}
									}
								}
							} else if (selectionDiemension == 1) {
								this.couleur = CouleurFigure.Couleur1;
								if (this.pos == 0 || this.pos == 2 || this.pos == 6 || this.pos == 8) {
									formeExte.style.backgroundColor = "var(--colorcarre)";
									this.couleur = CouleurFigure.Couleur2;
									if(FormeSelect.length > 1)
									{
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[1]);
										formeInte.classList.add(FormeSelect[1]);
										//verification triangle ou penda
										if(FormeSelect[1] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[1] == "penta"){
											formeInte.classList.add("caspentac");
										}
									}
								}
								if (this.pos == 1 || this.pos == 7) {
									formeExte.style.backgroundColor = "var(--colorone)";
									this.couleur = CouleurFigure.Couleur3;
									if(FormeSelect.length > 2)
									{
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[2]);
										formeInte.classList.add(FormeSelect[2]);
										//verification triangle ou penda
										if(FormeSelect[2] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[2] == "penta"){
											formeInte.classList.add("caspentac");
										}
									}
								}
								if (this.pos == 4) {
									formeExte.style.backgroundColor = "var(--colortwo)";
									this.couleur = CouleurFigure.Couleur4;
									if(FormeSelect.length > 3)
									{
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[3]);
										formeInte.classList.add(FormeSelect[3]);
										//verification triangle ou penda
										if(FormeSelect[3] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[3] == "penta"){
											formeInte.classList.add("caspentac");
										}
									} else if (FormeSelect.length > 1){
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[1]);
										formeInte.classList.add(FormeSelect[1]);
										//verification triangle ou penda
										if(FormeSelect[1] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[1] == "penta"){
											formeInte.classList.add("caspentac");
										}
									}
								}
							}
						}
						containInte.appendChild(formeInte);
						containInte.className = "containinteCarte";
						containExte.appendChild(formeExte);
						containExte.className = "containexteCarte";

						vidcontainer.appendChild(containInte);
						vidcontainer.appendChild(containExte);

						divcase.appendChild(vidcontainer);
						this.CodeHTML = divcase;
						break;

					case TypeFigure.Petit:

						var containInte = document.createElement('div');
						var formeInte = document.createElement('div');
						var containExte = document.createElement('div');
						var formeExte = document.createElement('div');

						var vidcontainer = document.createElement('div');
						vidcontainer.className = "containform3carte";
						vidcontainer.className = "proute";

						formeExte.className = "formeexte";
						formeExte.classList.add('rondv2');

						formeInte.className = "formeinte";
						formeInte.classList.add('rondv2');

						formeInte.style.backgroundColor = "var(--colorbase)";

						if (this.couleur != null) {
							if (this.couleur == CouleurFigure.Couleur1) {
								formeInte.style.backgroundColor = "var(--colorbase)";
							} else if (this.couleur == CouleurFigure.Couleur2) {
								formeInte.style.backgroundColor = "var(--colorcarre)";
								if(FormeSelect.length > 1)
								{
									//suppression formeBase
									formeExte.classList.remove('rondv2');
									formeInte.classList.remove('rondv2');
									//ajout forme
									formeExte.classList.add(FormeSelect[1]);
									formeInte.classList.add(FormeSelect[1]);
									//verification triangle ou penda
									if(FormeSelect[1] == "triangle"){
										formeInte.classList.add("castrianglec");
									}
									if(FormeSelect[1] == "penta"){
										formeInte.classList.add("caspentac");
									}
								}

							} else if (this.couleur == CouleurFigure.Couleur3) {
								formeInte.style.backgroundColor = "var(--colorone)";
								if(FormeSelect.length > 2)
								{
									//suppression formeBase
									formeExte.classList.remove('rondv2');
									formeInte.classList.remove('rondv2');
									//ajout forme
									formeExte.classList.add(FormeSelect[2]);
									formeInte.classList.add(FormeSelect[2]);
									//verification triangle ou penda
									if(FormeSelect[2] == "triangle"){
										formeInte.classList.add("castrianglec");
									}
									if(FormeSelect[2] == "penta"){
										formeInte.classList.add("caspentac");
									}
								}
							} else if (this.couleur == CouleurFigure.Couleur4) {
								formeInte.style.backgroundColor = "var(--colortwo)";
								if(FormeSelect.length > 3)
								{
									//suppression formeBase
									formeExte.classList.remove('rondv2');
									formeInte.classList.remove('rondv2');
									//ajout forme
									formeExte.classList.add(FormeSelect[3]);
									formeInte.classList.add(FormeSelect[3]);
									//verification triangle ou penda
									if(FormeSelect[3] == "triangle"){
										formeInte.classList.add("castrianglec");
									}
									if(FormeSelect[3] == "penta"){
										formeInte.classList.add("caspentac");
									}
								} else if (FormeSelect.length > 1){
									//suppression formeBase
									formeExte.classList.remove('rondv2');
									formeInte.classList.remove('rondv2');
									//ajout forme
									formeExte.classList.add(FormeSelect[1]);
									formeInte.classList.add(FormeSelect[1]);
									//verification triangle ou penda
									if(FormeSelect[1] == "triangle"){
										formeInte.classList.add("castrianglec");
									}
									if(FormeSelect[1] == "penta"){
										formeInte.classList.add("caspentac");
									}
								}
							}
						} else {
							this.couleur = CouleurFigure.Couleur1;
							if (selectionDiemension == 0) {
								this.couleur = CouleurFigure.Couleur1;
								if (this.pos == 0 || this.pos == 2 || this.pos == 9 || this.pos == 11) {
									formeInte.style.backgroundColor = "var(--colorcarre)";
									this.couleur = CouleurFigure.Couleur2;
									if(FormeSelect.length > 1)
									{
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[1]);
										formeInte.classList.add(FormeSelect[1]);
										//verification triangle ou penda
										if(FormeSelect[1] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[1] == "penta"){
											formeInte.classList.add("caspentac");
										}
									}
								}
								if (this.pos == 1 || this.pos == 10) {
									formeInte.style.backgroundColor = "var(--colorone)";
									this.couleur = CouleurFigure.Couleur3;
									if(FormeSelect.length > 2)
									{
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[2]);
										formeInte.classList.add(FormeSelect[2]);
										//verification triangle ou penda
										if(FormeSelect[2] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[2] == "penta"){
											formeInte.classList.add("caspentac");
										}
									}
								}
								if (this.pos == 4 || this.pos == 7) {
									formeInte.style.backgroundColor = "var(--colortwo)";
									this.couleur = CouleurFigure.Couleur4;
									if(FormeSelect.length > 3)
									{
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[3]);
										formeInte.classList.add(FormeSelect[3]);
										//verification triangle ou penda
										if(FormeSelect[3] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[3] == "penta"){
											formeInte.classList.add("caspentac");
										}
									} else if (FormeSelect.length > 1){
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[1]);
										formeInte.classList.add(FormeSelect[1]);
										//verification triangle ou penda
										if(FormeSelect[1] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[1] == "penta"){
											formeInte.classList.add("caspentac");
										}
									}
								}
							} else if (selectionDiemension == 1) {
								this.couleur = CouleurFigure.Couleur1;
								if (this.pos == 0 || this.pos == 2 || this.pos == 6 || this.pos == 8) {
									formeInte.style.backgroundColor = "var(--colorcarre)";
									this.couleur = CouleurFigure.Couleur2;
									if(FormeSelect.length > 1)
									{
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[1]);
										formeInte.classList.add(FormeSelect[1]);
										//verification triangle ou penda
										if(FormeSelect[1] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[1] == "penta"){
											formeInte.classList.add("caspentac");
										}
									}
								}
								if (this.pos == 1 || this.pos == 7) {
									formeInte.style.backgroundColor = "var(--colorone)";
									this.couleur = CouleurFigure.Couleur3;
									if(FormeSelect.length > 2)
									{
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[2]);
										formeInte.classList.add(FormeSelect[2]);
										//verification triangle ou penda
										if(FormeSelect[2] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[2] == "penta"){
											formeInte.classList.add("caspentac");
										}
									}
								}
								if (this.pos == 4) {
									formeInte.style.backgroundColor = "var(--colortwo)";
									this.couleur = CouleurFigure.Couleur4;
									if(FormeSelect.length > 3)
									{
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[3]);
										formeInte.classList.add(FormeSelect[3]);
										//verification triangle ou penda
										if(FormeSelect[3] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[3] == "penta"){
											formeInte.classList.add("caspentac");
										}
									} else if (FormeSelect.length > 1){
										//suppression formeBase
										formeExte.classList.remove('rondv2');
										formeInte.classList.remove('rondv2');
										//ajout forme
										formeExte.classList.add(FormeSelect[1]);
										formeInte.classList.add(FormeSelect[1]);
										//verification triangle ou penda
										if(FormeSelect[1] == "triangle"){
											formeInte.classList.add("castrianglec");
										}
										if(FormeSelect[1] == "penta"){
											formeInte.classList.add("caspentac");
										}
									}
								}
							}
						}

						containInte.appendChild(formeInte);
						containInte.className = "containinteCarte";
						containExte.appendChild(formeExte);
						containExte.className = "containexteCarte";

						vidcontainer.appendChild(containInte);
						vidcontainer.appendChild(containExte);

						divcase.appendChild(vidcontainer);
						this.CodeHTML = divcase;
						break;

				}
		}
	}
	get getHTML() {
		return this.CodeHTML;
	}
	get getX() {
		return this.X;
	}
	get getY() {
		return this.Y;
	}
	get getType() {
		return this.type;
	}
	get getForme() {
		return this.forme;
	}


}
const CouleurFigure = {
	Couleur1: 'c1',
	Couleur2: 'c2',
	Couleur3: 'c3',
	Couleur4: 'c4'
}

const FormeFigure = {
	Rond: 'rond',
	Carre: 'carre',
	Triangle: 'triangle',
	Pentagone: 'pentagone',
	Losange: 'Losange'
};

const TypeFigure = {
	Petit: 'p',
	Moyen: 'm',
	Grand: 'g'
};


function journuit() {
	//mode jour
	mode = mode + 1;
	if (mode % 2 == 0) {
		//changer logo swish
		document.getElementById("imglogo").src = "imgs/swishjour.png";
		document.getElementById("imglogo2").src = "imgs/swishjour.png";
		document.getElementById("contourbuttonvalider").style.backgroundColor = lightsedonca;
		//changer montagnesfond
		document.getElementById("mount1").src = "imgs/mount1.png";
		document.getElementById("mount2").src = "imgs/mount2.png";
		//nuages
		document.getElementById("cloud").src = "imgs/lightcloud.png";
		//maj couleur principales
		document.documentElement.style.setProperty('--lightprima', lightprima);
		document.documentElement.style.setProperty('--lightsedonca', lightsedonca);
		document.documentElement.style.setProperty('--lighttercia', lighttercia);
	}
	//mode nuit
	else {
		//changer logo swish
		document.getElementById("imglogo").src = "imgs/swishnuit.png";
		document.getElementById("imglogo2").src = "imgs/swishnuit.png";
		document.getElementById("contourbuttonvalider").style.backgroundColor = darkseconda;
		//changer montagnesfond
		document.getElementById("mount1").src = "imgs/darkmount1.png";
		document.getElementById("mount2").src = "imgs/darkmount2.png";
		//nuages
		document.getElementById("cloud").src = "imgs/darkcloud.png";
		//maj couleur principales
		document.documentElement.style.setProperty('--lightprima', darkprima);
		document.documentElement.style.setProperty('--lightsedonca', darkseconda);
		document.documentElement.style.setProperty('--lighttercia', darktercia);
	}
}

//===================================================================================
//FONCTION GESTION DE GAME

function lancerpartie() {

	lesPoints = 0;

	document.getElementById("progressbar").style.visible = "hidden";
	document.getElementById('table_HS_Timer').style.display = "none";
	document.getElementById('table_HS_Match').style.display = "none";
	var ready = true;

	//fonction de creation de partie a changer en fonction du test voulu
	if (selectionMode == 0) {
		goAide();
		if (selectionDiemension == 0) {
			majSizeCard(3);
			creePartieClassique3_4();
		} else if (selectionDiemension == 1) {
			majSizeCard(3);
			creePartieClassique3_3();
			majSizeCard(3);
		} else if (selectionDiemension == 2) {
			majSizeCard(2);
			creePartieClassique3_2();
		} else if (selectionDiemension == 3) {

			if(!modelangue){
				window.alert("Oups, il semblerait que ce mode ne soit pas encore disponible...");
			}
			else{
				window.alert("Oops, it seems that this mode is not yet available...");
			}


			ready = false;
		}

	} else if (selectionMode == 1) {
		goAide();
		document.getElementById("progressbar").style.visibility = "visible";
		decompte();
		if (selectionDiemension == 0) {
			creePartieInfini(4, 3);
			majSizeCard(3);
		} else if (selectionDiemension == 1) {
			creePartieInfini(3, 3);
			majSizeCard(3);
		} else if (selectionDiemension == 2) {
			creePartieInfini3_2(3, 2);
			majSizeCard(2);
		} else if (selectionDiemension == 3) {
			creePartieInfini3_2(2, 2);
			majSizeCard(2);
		}
	} else if (selectionMode == 2) {
		goAide();
		if (selectionDiemension == 0) {
			creePartieInfini(4, 3);
			majSizeCard(3);
		} else if (selectionDiemension == 1) {
			creePartieInfini(3, 3);
			majSizeCard(3);
		} else if (selectionDiemension == 2) {
			creePartieInfini3_2(3, 2);
			majSizeCard(2);
		} else if (selectionDiemension == 3) {
			creePartieInfini3_2(2, 2);
			majSizeCard(2);
		}
	} else if(selectionMode == 3){
		document.getElementById("pageAccueil").style.visibility = "hidden";
		//document.getElementById("PageLobbyMulti").style.visibility = "visible";

	}
	if (ready) {
		document.getElementById("pageAccueil").style.visibility = "hidden";
		document.getElementById("pageGame").style.visibility = "visible";
	}
}

function retour() {
	document.getElementById("pageAccueil").style.visibility = "visible";
	document.getElementById("pageGame").style.visibility = "hidden";
	window.location.reload();
}

function rechargerGAME() {
	while (document.getElementById("containcards").firstElementChild != null) {
		document.getElementById("containcards").firstElementChild.remove();
	}
	for (var i = 0; i < deckPartie.length; i++) {
		document.getElementById("containcards").appendChild(deckPartie[i].getHTML);
	}
}

function redistribuerPlateau() {
	lesPoints -= 5;
	document.getElementById("affPoints").textContent = 'Score : ' + lesPoints;
	if (selectionMode == 0) {
		redistribuer();
	} else if (selectionMode == 1 || selectionMode == 2) {
		for (var i = 0; i < deckPartie.length; i++) {
			if (selectionDiemension == 2) {
				remplacerLaCarte3_2(deckPartie[i]);
			} else if (selectionDiemension == 3) {
				remplacerLaCarte2_2(deckPartie[i]);
			} else {
				remplacerLaCarte(deckPartie[i]);
			}
		}
	}
	if(selectionMode == 3){
		redistribuerMulti();
	}
	afficherCartes(deckPartie);
}

function redistribuerMulti() {
	carteselect=[];
	carteselectPos=[];
	reselectCartes();

	deckPartie = [];
	for (var i = 0; i < 16; i++) {
		var index = getRandom(0, TasDuJEU.length - 1)
		var dejaPresente = false;//Permet de savoir si la carte est deja dans la liste
		for (var j = 0; j < deckPartie.length; j++) {
			if (TasDuJEU[index].identifiant == deckPartie[j].identifiant) {
				dejaPresente = true;
			}
		}
		while (dejaPresente == true) {
			index = getRandom(0, TasDuJEU.length - 1)
			dejaPresente = false;//Permet de savoir si la carte est deja dans la liste
			for (var j = 0; j < deckPartie.length; j++) {
				if (TasDuJEU[index].identifiant == deckPartie[j].identifiant) {
					dejaPresente = true;
				}
			}
		}

		deckPartie.push(copieCarte(TasDuJEU[index]));
	}
	//Envoyer les 16 cartes du plateau
	listecartes=[];
	var uneMatrice;
	var ajt;
	/*
	for (var i = 0; i < deckPartie.length; i++) {
		var ajt = deckPartie[i].getMatrice[0] + deckPartie[i].getMatrice[1]  + deckPartie[i].getMatrice[2];
		listecartes.push(ajt);
		console.log(ajt);
	}*/
	for (var j = 0; j < deckPartie.length; j++) { //Pour toutes les cartes de deckPartie

		uneMatrice = creaMatriceVierge(deckPartie[j].row, deckPartie[j].column); //Création d'une matrice vierge ( Tableau 2D avec que des 0)

		for (var i = 0; i < deckPartie[j].SesFigures.length; i++) { //Pour chaque figure présente dans la carte

			if (deckPartie[j].SesFigures[i].type == TypeFigure.Petit) { //Check de la taille de la figure et adaptation de la valeur dans la matrice
				uneMatrice[deckPartie[j].SesFigures[i].X][deckPartie[j].SesFigures[i].Y] = 1;
			} else if (deckPartie[j].SesFigures[i].type == TypeFigure.Moyen) {
				uneMatrice[deckPartie[j].SesFigures[i].X][deckPartie[j].SesFigures[i].Y] = 2;
			}
		}

		ajt = uneMatrice[0]+uneMatrice[1]+uneMatrice[2];//une fois matrice vierge complétée on transforme ça en data
		listecartes.push(ajt);
		//console.log(ajt);
	}
	//console.log("LISTE BUG2 ",listecartes);
	$(document).ready(function () {
		var cartestables = listecartes;

		$.ajax({
			data: {
				cartestables: cartestables
			},
			type: "post",
			url: "envoyercartes.php",
			success: function (data) {
				console.log("====Les cartes ont été envoyés (redistribuer)===")
			}
		});
	});
}

function redistribuer() {
	deckPartie = [];
	for (var i = 0; i < 16; i++) {
		var index = getRandom(0, TasDuJEU.length - 1)
		var dejaPresente = false;//Permet de savoir si la carte est deja dans la liste
		for (var j = 0; j < deckPartie.length; j++) {
			if (TasDuJEU[index].identifiant == deckPartie[j].identifiant) {
				dejaPresente = true;
			}
		}
		while (dejaPresente == true) {
			index = getRandom(0, TasDuJEU.length - 1)
			dejaPresente = false;//Permet de savoir si la carte est deja dans la liste
			for (var j = 0; j < deckPartie.length; j++) {
				if (TasDuJEU[index].identifiant == deckPartie[j].identifiant) {
					dejaPresente = true;
				}
			}
		}

		deckPartie.push(copieCarte(TasDuJEU[index]));
	}
	afficherCartes(deckPartie);
}

//================================================================================================================================================================
//Fonction creations de game

function genererTouteslesCartes3_2Possibles() {
	deckPartie = [];
	carteselect = [];

	while (document.getElementById("containcards").firstElementChild != null) {
		document.getElementById("containcards").firstElementChild.remove();
	}
	for (var i = 0; i < 2; i++) {
		for (var j = 0; j < 2; j++) {
			for (var k = 0; k < 6; k++) {

				//Création de la grande figure
				for (var h = 0; h < 2; h++) {
					var AllFigure = [];

					//Création de la petite figure en fonction de sa place et de la boucle
					if (i == 0) {
						if (j == 0) {
							var uneFigure = new Figure(TypeFigure.Petit, FormeFigure.Rond, 0, i, CouleurFigure.Couleur2);
						} else {
							var uneFigure = new Figure(TypeFigure.Petit, FormeFigure.Rond, 0, i, CouleurFigure.Couleur3);
						}
					} else {
						if (j == 0) {
							var uneFigure = new Figure(TypeFigure.Petit, FormeFigure.Rond, 0, i, CouleurFigure.Couleur1);
						} else {
							var uneFigure = new Figure(TypeFigure.Petit, FormeFigure.Rond, 0, i, CouleurFigure.Couleur4);
						}
					}

					AllFigure.push(uneFigure);

					//Verification de l'effet mirroir sur les cartes ayant un point central
					var mirroir = true;
					if (Math.floor(k / 2) == 2 && i == 1) {
						mirroir = false;
					}

					if (k != i * 2) {

						if (k == 2 || k == 3) {

							if (h == 0) {

								var uneMoyenFigure = new Figure(TypeFigure.Moyen, FormeFigure.Rond, k % 2, Math.floor(k / 2), CouleurFigure.Couleur1);
							} else {

								var uneMoyenFigure = new Figure(TypeFigure.Moyen, FormeFigure.Rond, k % 2, Math.floor(k / 2), CouleurFigure.Couleur4);
							}

						} else {
							if (h == 0) {

								var uneMoyenFigure = new Figure(TypeFigure.Moyen, FormeFigure.Rond, k % 2, Math.floor(k / 2), CouleurFigure.Couleur2);
							} else {

								var uneMoyenFigure = new Figure(TypeFigure.Moyen, FormeFigure.Rond, k % 2, Math.floor(k / 2), CouleurFigure.Couleur3);
							}

						}
						if (mirroir) {
							AllFigure.push(uneMoyenFigure);
							var uneCarte = new Carte(deckPartie.length + 1, AllFigure, 3, 2);
							deckPartie.push(uneCarte);
						}
					}

				}

			}
		}
	}
	return deckPartie;
}


function changerlesCartes3_2() {
	while (carteselect.length != 0) {
		remplacerLaCarte3_2(carteselect[0]);
		carteselect.shift();
	}
	rechargerGAME();
}

function changerlesCartes2_2() {
	while (carteselect.length != 0) {
		remplacerLaCarte2_2(carteselect[0]);
		carteselect.shift();
	}
	rechargerGAME();
}

function remplacerLaCarte2_2(uneCarte) {
	pos = deckPartie.indexOf(uneCarte);
	var AllFigure = [];
	var Cox1, Coy1, Cox2, Coy2;

	var numCouleur = getRandom(0, 3);
	var laCouleur;

	Cox1 = getRandom(0, uneCarte.column - 1);
	Coy1 = getRandom(0, uneCarte.row - 1);

	if (numCouleur == 0) {
		laCouleur = CouleurFigure.Couleur1;
	} else if (numCouleur == 1) {
		laCouleur = CouleurFigure.Couleur2;
	} else if (numCouleur == 2) {
		laCouleur = CouleurFigure.Couleur3;
	} else if (numCouleur == 3) {
		laCouleur = CouleurFigure.Couleur4;
	}
	AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, Cox1, Coy1, laCouleur));

	Cox2 = getRandom(0, uneCarte.column - 1);
	Coy2 = getRandom(0, uneCarte.row - 1);

	while (Cox2 == Cox1 && Coy1 == Coy2) {
		Cox2 = getRandom(0, uneCarte.column - 1);
		Coy2 = getRandom(0, uneCarte.row - 1);
	}
	numCouleur = getRandom(0, 3);

	if (numCouleur == 0) {
		laCouleur = CouleurFigure.Couleur1;
	} else if (numCouleur == 1) {
		laCouleur = CouleurFigure.Couleur2;
	} else if (numCouleur == 2) {
		laCouleur = CouleurFigure.Couleur3;
	} else if (numCouleur == 3) {
		laCouleur = CouleurFigure.Couleur4;
	}
	AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, Cox2, Coy2, laCouleur));
	//Code de Generation de Figure
	var newCarte = new Carte(pos + 1, AllFigure, uneCarte.row, uneCarte.column);
	deckPartie[pos] = newCarte;
}

function remplacerLaCarte3_2(uneCarte) {
	pos = deckPartie.indexOf(uneCarte);
	var AllFigure = [];
	var Cox1, Coy1, Cox2, Coy2;

	var numCouleur = getRandom(0, 1);
	var laCouleur;

	Cox1 = getRandom(0, uneCarte.column - 1);
	Coy1 = getRandom(0, uneCarte.row - 1);
	if (Cox1 + 2 * Coy1 == 2 || Cox1 + 2 * Coy1 == 3) {
		if (numCouleur == 0) {
			laCouleur = CouleurFigure.Couleur1;
		} else {
			laCouleur = CouleurFigure.Couleur4;
		}
	} else {
		if (numCouleur == 0) {
			laCouleur = CouleurFigure.Couleur2;
		} else {
			laCouleur = CouleurFigure.Couleur3;
		}
	}
	AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, Cox1, Coy1, laCouleur));

	Cox2 = getRandom(0, uneCarte.column - 1);
	Coy2 = getRandom(0, uneCarte.row - 1);

	while (Cox2 == Cox1 && Coy1 == Coy2) {
		Cox2 = getRandom(0, uneCarte.column - 1);
		Coy2 = getRandom(0, uneCarte.row - 1);
	}
	if (Cox2 + 2 * Coy2 == 2 || Cox2 + 2 * Coy2 == 3) {
		if (numCouleur == 0) {
			laCouleur = CouleurFigure.Couleur1;
		} else {
			laCouleur = CouleurFigure.Couleur4;
		}
	} else {
		if (numCouleur == 0) {
			laCouleur = CouleurFigure.Couleur2;
		} else {
			laCouleur = CouleurFigure.Couleur3;
		}
	}
	AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, Cox2, Coy2, laCouleur));
	//Code de Generation de Figure
	var newCarte = new Carte(pos + 1, AllFigure, uneCarte.row, uneCarte.column);
	deckPartie[pos] = newCarte;
}

function creePartieInfini3_2(nbRow, nbColum) {
	deckPartie = [];
	carteselect = [];
	while (document.getElementById("containcards").firstElementChild != null) {
		document.getElementById("containcards").firstElementChild.remove();
	}
	for (var j = 1; j <= 16; j++) {

		var numCouleur = getRandom(0, 1);
		var laCouleur;
		var AllFigure = [];
		var Cox1, Coy1, Cox2, Coy2;

		Cox1 = getRandom(0, nbColum - 1);
		Coy1 = getRandom(0, nbRow - 1);
		if (Cox1 + 2 * Coy1 == 2 || Cox1 + 2 * Coy1 == 3) {
			if (numCouleur == 0) {
				laCouleur = CouleurFigure.Couleur1;
			} else {
				laCouleur = CouleurFigure.Couleur4;
			}
		} else {
			if (numCouleur == 0) {
				laCouleur = CouleurFigure.Couleur2;
			} else {
				laCouleur = CouleurFigure.Couleur3;
			}
		}
		AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, Cox1, Coy1, laCouleur));

		Cox2 = getRandom(0, nbColum - 1);
		Coy2 = getRandom(0, nbRow - 1);

		while (Cox2 == Cox1 && Coy1 == Coy2) {
			Cox2 = getRandom(0, nbColum - 1);
			Coy2 = getRandom(0, nbRow - 1);
		}

		if (Cox2 + 2 * Coy2 == 2 || Cox2 + 2 * Coy2 == 3) {
			if (numCouleur == 0) {
				laCouleur = CouleurFigure.Couleur1;
			} else {
				laCouleur = CouleurFigure.Couleur4;
			}
		} else {
			if (numCouleur == 0) {
				laCouleur = CouleurFigure.Couleur2;
			} else {
				laCouleur = CouleurFigure.Couleur3;
			}
		}

		AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, Cox2, Coy2, laCouleur));
		//Code de Generation de Figure
		var uneCarte = new Carte(j, AllFigure, nbRow, nbColum);

		deckPartie.push(uneCarte);

		document.getElementById("containcards").appendChild(uneCarte.link);
	}
}

function MaFonction() {
	var copyText = document.getElementById("Monidpartie");
	copyText.select();
	copyText.setSelectionRange(0, 99999);
	document.execCommand("copy");
}

function testerCartesMulti() {
	//try {
	if (carteselect.length == 0) {
		window.alert("Selection Vide test pour jeu classique");
		return;
	} else if (carteselect.length < 2) {
		window.alert("Selection Trop Petite");
		return;
	} else {
		var copie = [];
		for (var i = 0; i < carteselect.length; i++) {
			copie.push(copieCarte(carteselect[i]));
		}
		var tab = [];
		tab = AssemblageARBRE(copie, copieCarte(carteselect[0]));
		var solution = true;
		var tabCode = [];
		for (var i = 0; i < tab.length; i++) {
			tabCode.push(tab[i].code);
		}

		for (var i = 0; i < tab.length; i++) {
			if (tab[i].code == carteselect.length) {
				solution = false;
				cpt = cpt + 20 * copie.length;
				lesPoints += carteselect.length;
				document.getElementById("affPoints").textContent = 'Score : ' + lesPoints;
				var new2Cartes=changerlesCartesDeTasDeJeuMulti();
			}
		}
		if (solution) {
			document.getElementById("affSolution").textContent = 'Aucune solution trouvée...';
		}
		if (!solution) {
			document.getElementById("affSolution").textContent = 'Assemblage de ' + copie.length + ' cartes trouvé ! Il reste encore ' + TasDuJEU.length + ' cartes !';
			carteselectPos=[];
			carteselect=[];
			reselectCartes();
		}
	}
	/*} catch (e) {
		window.alert(e);
	}*/

	//Envoyer les 16 cartes du plateau
	listecartes=[];
	var ajt;
	/*
	for (var i = 0; i < deckPartie.length; i++) {
		var ajt = deckPartie[i].getMatrice[0] + deckPartie[i].getMatrice[1]  + deckPartie[i].getMatrice[2];
		listecartes.push(ajt);
		console.log(ajt);
	}*/
	for (var j = 0; j < deckPartie.length; j++) { //Pour toutes les cartes de deckPartie
		var uneMatrice = creaMatriceVierge(deckPartie[j].row, deckPartie[j].column); //Création d'une matrice vierge ( Tableau 2D avec que des 0)


		//CODE A RISQUE
		for (var i = 0; i < deckPartie[j].SesFigures.length; i++) { //Pour chaque figure présente dans la carte

			if (deckPartie[j].SesFigures[i].type[0] == TypeFigure.Petit) { //Check de la taille de la figure et adaptation de la valeur dans la matrice
				uneMatrice[deckPartie[j].SesFigures[i].X][deckPartie[j].SesFigures[i].Y] = 1;
			} else if (deckPartie[j].SesFigures[i].type[0] == TypeFigure.Moyen) {
				uneMatrice[deckPartie[j].SesFigures[i].X][deckPartie[j].SesFigures[i].Y] = 2;
			}
		}
		//


		ajt = uneMatrice[0]+uneMatrice[1]+uneMatrice[2];//une fois matrice vierge complétée on transforme ça en data
		listecartes.push(ajt);
		//console.log(ajt);
	}
	//console.log("LISTE BUG1: ",listecartes);

	$(document).ready(function () {
		var cartestables = listecartes;

		$.ajax({
			data: {
				cartestables: cartestables
			},
			type: "post",
			url: "envoyercartes.php",
			success: function (data) {
				console.log("====Les cartes ont été envoyés (valider cartes)===")
			}
		});
	});
}
function changerlesCartesDeTasDeJeuMulti() {
	var new2cartes = [];
	// Pour toutes les cartes selectionnées
	while (carteselectPos.length != 0) {

		// //Enlever la carte dans le tas du jeu
		// var unIndex = TasDuJEU.indexOf(carteselect[0]);
		// TasDuJEU.splice(unIndex, 1);

		//si il reste plus de 16 cartes
		if (TasDuJEU.length > 16) {
			//On prend une autre carte aléatoire dans tas du jeu
			var index2 = getRandom(0, TasDuJEU.length - 1)
			//var dejaPresente = false;
			//Permet de savoir si la carte est deja dans la liste
			// for (var j = 0; j < deckPartie.length; j++) {
			// 	if (TasDuJEU[index2].identifiant == deckPartie[j].identifiant) {
			// 		dejaPresente = true;
			// 	}
			// }

			deckPartie[carteselectPos[0]]=TasDuJEU[index2];
			TasDuJEU.splice(index2,1);


			//on recommmence jusqu'à obtenri une carte pas dans le tas de jeu
			// while (dejaPresente == true) {
			// 	index2 = getRandom(0, TasDuJEU.length - 1)
			// 	dejaPresente = false;//Permet de savoir si la carte est deja dans la liste
			// 	for (var j = 0; j < deckPartie.length; j++) {
			// 		if (TasDuJEU[index2].identifiant == deckPartie[j].identifiant) {
			// 			dejaPresente = true;
			// 		}
			// 	}
			// }

			// unIndex = deckPartie.indexOf(carteselect[0]);
			// deckPartie[unIndex] = TasDuJEU[index2];
			// new2cartes.push(TasDuJEU[index2]);
		} else {
			// si il y a moins de 16 cartes on enlève juste
			deckPartie.splice(carteselect[0],1);
			// unIndex = deckPartie.indexOf(carteselect[0]);
			// deckPartie.splice(unIndex, 1);
		}
		carteselectPos.shift();
		carteselect.shift();
	}
	afficherCartes(deckPartie);
	return new2cartes;
}

function creePartieClassique3_2() {
	TasDuJEU = genererTouteslesCartes3_2Possibles();
	deckPartie = [];
	for (var i = 0; i < TasDuJEU.length; i++) {
		CodeRotation = getRandom(0, 3);
		if (CodeRotation == 0) {
			TasDuJEU[i] = TasDuJEU[i];
		} else if (CodeRotation == 1) {
			TasDuJEU[i] = Horizontale(TasDuJEU[i]);
		} else if (CodeRotation == 2) {
			TasDuJEU[i] = Verticale(TasDuJEU[i]);
		} else if (CodeRotation == 3) {
			TasDuJEU[i] = Verticale(TasDuJEU[i]);
			TasDuJEU[i] = Horizontale(TasDuJEU[i]);
		}
	}
	for (var i = 0; i < 16; i++) {
		var index = getRandom(0, TasDuJEU.length - 1)
		var dejaPresente = false;//Permet de savoir si la carte est deja dans la liste
		for (var j = 0; j < deckPartie.length; j++) {
			if (TasDuJEU[index].identifiant == deckPartie[j].identifiant) {
				dejaPresente = true;
			}
		}
		while (dejaPresente == true) {
			index = getRandom(0, TasDuJEU.length - 1)
			dejaPresente = false;//Permet de savoir si la carte est deja dans la liste
			for (var j = 0; j < deckPartie.length; j++) {
				if (TasDuJEU[index].identifiant == deckPartie[j].identifiant) {
					dejaPresente = true;
				}
			}
		}

		deckPartie.push(copieCarte(TasDuJEU[index]));
	}
	afficherCartes(deckPartie);
}


function genererTouteslesCartes3_3Possibles() {
	deckPartie = [];
	carteselect = [];

	while (document.getElementById("containcards").firstElementChild != null) {
		document.getElementById("containcards").firstElementChild.remove();
	}
	for (var i = 1; i < 9; i++) {
		var AllFigure = [];
		AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, 0, 0));
		AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, i % 3, Math.floor(i / 3)));
		var uneCarte = new Carte(deckPartie.length + 1, AllFigure, 3, 3);
		deckPartie.push(uneCarte);
		var AllFigure = [];
		AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, 0, 0));
		AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, i % 3, Math.floor(i / 3)));
		var uneCarte = new Carte(deckPartie.length + 1, AllFigure, 3, 3);
		deckPartie.push(uneCarte);
	}
	for (var i = 0; i < 9; i++) {
		if (i != 3 && Math.floor(i / 3) != 2) {
			var AllFigure = [];
			AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, 0, 1));
			AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, i % 3, Math.floor(i / 3)));
			var uneCarte = new Carte(deckPartie.length + 1, AllFigure, 3, 3);
			deckPartie.push(uneCarte);
			var AllFigure = [];
			AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, 0, 1));
			AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, i % 3, Math.floor(i / 3)));
			var uneCarte = new Carte(deckPartie.length + 1, AllFigure, 3, 3);
			deckPartie.push(uneCarte);
		}
	}
	for (var i = 0; i < 9; i++) {
		if (i != 1 && (i % 3) != 2) {
			var AllFigure = [];
			AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, 1, 0));
			AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, i % 3, Math.floor(i / 3)));
			var uneCarte = new Carte(deckPartie.length + 1, AllFigure, 3, 3);
			deckPartie.push(uneCarte);
			var AllFigure = [];
			AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, 1, 0));
			AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, i % 3, Math.floor(i / 3)));
			var uneCarte = new Carte(deckPartie.length + 1, AllFigure, 3, 3);
			deckPartie.push(uneCarte);
		}
	}
	for (var i = 0; i < 4; i++) {
		if (i != 2) {
			var AllFigure = [];
			AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, 1, 1));
			AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, i % 3, Math.floor(i / 3)));
			var uneCarte = new Carte(deckPartie.length + 1, AllFigure, 3, 3);
			deckPartie.push(uneCarte);
			var AllFigure = [];
			AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, 1, 1));
			AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, i % 3, Math.floor(i / 3)));
			var uneCarte = new Carte(deckPartie.length + 1, AllFigure, 3, 3);
			deckPartie.push(uneCarte);
		}
	}
	return deckPartie;

}

function creePartieClassique3_3() {
	TasDuJEU = genererTouteslesCartes3_3Possibles();
	deckPartie = [];
	for (var i = 0; i < TasDuJEU.length; i++) {
		CodeRotation = getRandom(0, 3);
		if (CodeRotation == 0) {
			TasDuJEU[i] = TasDuJEU[i];
		} else if (CodeRotation == 1) {
			TasDuJEU[i] = Horizontale(TasDuJEU[i]);
		} else if (CodeRotation == 2) {
			TasDuJEU[i] = Verticale(TasDuJEU[i]);
		} else if (CodeRotation == 3) {
			TasDuJEU[i] = Verticale(TasDuJEU[i]);
			TasDuJEU[i] = Horizontale(TasDuJEU[i]);
		}
	}
	for (var i = 0; i < 16; i++) {
		var index = getRandom(0, TasDuJEU.length - 1)
		var dejaPresente = false;//Permet de savoir si la carte est deja dans la liste
		for (var j = 0; j < deckPartie.length; j++) {
			if (TasDuJEU[index].identifiant == deckPartie[j].identifiant) {
				dejaPresente = true;
			}
		}
		while (dejaPresente == true) {
			index = getRandom(0, TasDuJEU.length - 1)
			dejaPresente = false;//Permet de savoir si la carte est deja dans la liste
			for (var j = 0; j < deckPartie.length; j++) {
				if (TasDuJEU[index].identifiant == deckPartie[j].identifiant) {
					dejaPresente = true;
				}
			}
		}

		deckPartie.push(copieCarte(TasDuJEU[index]));
	}
	afficherCartes(deckPartie);
}

function genererTouteslesCartes3_4Possibles() {
	deckPartie = [];
	carteselect = [];
	while (document.getElementById("containcards").firstElementChild != null) {
		document.getElementById("containcards").firstElementChild.remove();
	}

	for (var j = 0; j < 2; j++) {
		for (var i = 0; i < 12; i++) {
			var AllFigure = [];
			AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, 0, j));
			if (i != 3 * j) {
				AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, i % 3, Math.floor(i / 3)));
				var uneCarte = new Carte(deckPartie.length + 1, AllFigure, 4, 3);
				deckPartie.push(uneCarte);
				if (!doublonInterdit(uneCarte)) {
					AllFigure = [];
					AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, 0, j));
					AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, i % 3, Math.floor(i / 3)));
					var unDoublon = new Carte(deckPartie.length + 1, AllFigure, 4, 3);
					deckPartie.push(unDoublon);
				}
			}
		}
	}
	for (var j = 0; j < 2; j++) {
		for (var i = 0; i < 12; i++) {
			var AllFigure = [];
			AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, 1, j));
			if (i != 3 * j + 1) {
				if (i != 2 && i != 5 && i != 8 && i != 11) {
					AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, i % 3, Math.floor(i / 3)));
					var uneCarte = new Carte(deckPartie.length + 1, AllFigure, 4, 3);
					deckPartie.push(uneCarte);
					if (!doublonInterdit(uneCarte)) {
						AllFigure = [];
						AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, 1, j));
						AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, i % 3, Math.floor(i / 3)));
						var unDoublon = new Carte(deckPartie.length + 1, AllFigure, 4, 3);
						deckPartie.push(unDoublon);
					}
				}
			}
		}
	}
	return deckPartie;
}

function afficherCartes(Liste) {
	while (document.getElementById("containcards").firstElementChild != null) {
		document.getElementById("containcards").firstElementChild.remove();
	}
	for (var i = 0; i < Liste.length; i++) {
		document.getElementById("containcards").appendChild(copieCarte(Liste[i]).link);
	}
}

function creePartieClassique3_4() {
	TasDuJEU = genererTouteslesCartes3_4Possibles();
	deckPartie = [];
	for (var i = 0; i < TasDuJEU.length; i++) {
		CodeRotation = getRandom(0, 3);
		if (CodeRotation == 0) {
			TasDuJEU[i] = TasDuJEU[i];
		} else if (CodeRotation == 1) {
			TasDuJEU[i] = Horizontale(TasDuJEU[i]);
		} else if (CodeRotation == 2) {
			TasDuJEU[i] = Verticale(TasDuJEU[i]);
		} else if (CodeRotation == 3) {
			TasDuJEU[i] = Verticale(TasDuJEU[i]);
			TasDuJEU[i] = Horizontale(TasDuJEU[i]);
		}
	}
	for (var i = 0; i < 16; i++) {
		var index = getRandom(0, TasDuJEU.length - 1)
		var dejaPresente = false;//Permet de savoir si la carte est deja dans la liste
		for (var j = 0; j < deckPartie.length; j++) {
			if (TasDuJEU[index].identifiant == deckPartie[j].identifiant) {
				dejaPresente = true;
			}
		}
		while (dejaPresente == true) {
			index = getRandom(0, TasDuJEU.length - 1)
			dejaPresente = false;//Permet de savoir si la carte est deja dans la liste
			for (var j = 0; j < deckPartie.length; j++) {
				if (TasDuJEU[index].identifiant == deckPartie[j].identifiant) {
					dejaPresente = true;
				}
			}
		}

		deckPartie.push(copieCarte(TasDuJEU[index]));
	}
	afficherCartes(deckPartie);
}

function creePartieInfini(nbRow, nbColum) {
	deckPartie = [];
	carteselect = [];
	while (document.getElementById("containcards").firstElementChild != null) {
		document.getElementById("containcards").firstElementChild.remove();
	}
	for (var j = 1; j <= 16; j++) {

		var AllFigure = [];
		var Cox1, Coy1, Cox2, Coy2;

		Cox1 = getRandom(0, nbColum - 1);
		Coy1 = getRandom(0, nbRow - 1);
		AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, Cox1, Coy1));

		Cox2 = getRandom(0, nbColum - 1);
		Coy2 = getRandom(0, nbRow - 1);

		while (Cox2 == Cox1 && Coy1 == Coy2) {
			Cox2 = getRandom(0, nbColum - 1);
			Coy2 = getRandom(0, nbRow - 1);
		}
		AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, Cox2, Coy2));
		//Code de Generation de Figure
		var uneCarte = new Carte(j, AllFigure, nbRow, nbColum);

		deckPartie.push(uneCarte);

		document.getElementById("containcards").appendChild(uneCarte.link);
	}
}

//========================================================================================================================================================
//Changer les cartes et Changer les cartes de Tas de jeu

function changerlesCartesDeTasDeJeu() {
	while (carteselect.length != 0) {
		var unIndex = TasDuJEU.indexOf(carteselect[0]);
		TasDuJEU.splice(unIndex, 1);
		if (TasDuJEU.length > 16) {
			var index2 = getRandom(0, TasDuJEU.length - 1)
			var dejaPresente = false;//Permet de savoir si la carte est deja dans la liste
			for (var j = 0; j < deckPartie.length; j++) {
				if (TasDuJEU[index2].identifiant == deckPartie[j].identifiant) {
					dejaPresente = true;
				}
			}
			while (dejaPresente == true) {
				index2 = getRandom(0, TasDuJEU.length - 1)
				dejaPresente = false;//Permet de savoir si la carte est deja dans la liste
				for (var j = 0; j < deckPartie.length; j++) {
					if (TasDuJEU[index2].identifiant == deckPartie[j].identifiant) {
						dejaPresente = true;
					}
				}
			}
			unIndex = deckPartie.indexOf(carteselect[0]);
			deckPartie[unIndex] = TasDuJEU[index2];
		} else {
			unIndex = deckPartie.indexOf(carteselect[0]);
			deckPartie.splice(unIndex, 1);
		}
		carteselect.shift();
	}
	afficherCartes(deckPartie);
}



function changerlesCartes() {
	while (carteselect.length != 0) {
		remplacerLaCarte(carteselect[0]);
		carteselect.shift();
	}
	rechargerGAME();
}

function remplacerLaCarte(uneCarte) {
	pos = deckPartie.indexOf(uneCarte);
	var AllFigure = [];
	var Cox1, Coy1, Cox2, Coy2;

	Cox1 = getRandom(0, uneCarte.column - 1);
	Coy1 = getRandom(0, uneCarte.row - 1);
	AllFigure.push(new Figure(new Array(TypeFigure.Petit), FormeFigure.Rond, Cox1, Coy1));

	Cox2 = getRandom(0, uneCarte.column - 1);
	Coy2 = getRandom(0, uneCarte.row - 1);

	while (Cox2 == Cox1 && Coy1 == Coy2) {
		Cox2 = getRandom(0, uneCarte.column - 1);
		Coy2 = getRandom(0, uneCarte.row - 1);
	}
	AllFigure.push(new Figure(new Array(TypeFigure.Moyen), FormeFigure.Rond, Cox2, Coy2));
	//Code de Generation de Figure
	var newCarte = new Carte(pos + 1, AllFigure, uneCarte.row, uneCarte.column);
	deckPartie[pos] = newCarte;
}

//====================================================================================
//GESTION DE MATRICE

function somMatrice(matrice1, matrice2, row, column) {
	var somMatrice = creaMatriceVierge(row, column);
	for (var i = 0; i < column; i++) {
		for (var j = 0; j < row; j++) {
			somMatrice[i][j] = matrice1[i][j] + matrice2[i][j];
		}
	}
	return somMatrice;
}

function copieMatrice(laMatrice, row, column) {
	var myMatrice = new Array();
	for (var i = 0; i < column; i++) {
		myMatrice[i] = new Array();
		for (var j = 0; j < row; j++) {
			myMatrice[i][j] = laMatrice[i][j];
		}
	}
	return myMatrice;
}

function getRandom(min, max) {
	return Math.round(Math.random() * (max - min) + min)
}

function creaMatriceVierge(row, column) {
	var myMatrice = new Array();
	for (var i = 0; i < column; i++) {
		myMatrice[i] = new Array();
		for (var j = 0; j < row; j++) {
			myMatrice[i][j] = 0;
		}
	}
	return myMatrice;
}

function creaMatrice(row, column) {

	var myMatrice = creaMatriceVierge(row, column);
	//window.alert(myMatrice)

	var Cox1, Coy1, Cox2, Coy2;

	Cox1 = getRandom(0, column - 1);
	Coy1 = getRandom(0, row - 1);

	myMatrice[Cox1][Coy1] = 1;

	Cox2 = getRandom(0, column - 1);
	Coy2 = getRandom(0, row - 1);

	while (Cox2 == Cox1 && Coy1 == Coy2) {
		Cox2 = getRandom(0, column - 1);
		Coy2 = getRandom(0, row - 1);
	}
	myMatrice[Cox2][Coy2] = 2;

	return myMatrice;

}

//=================================================================================
//chercheCombi => Compte les combinainsons de 2 à 5 cartes a l'ecran
//Pour changer le nombre de carte maximum dans une liste il faut changer la valeur max de N

function chercherCombinaison() {
	var Cptsolution = 0;
	/*
	 Pour toutes les longueurs possible d’un tas de carte (N = 0, N<4 ; N++)
	Pour toutes les échantillon de cartes ( i = 0 ; i<16 – N ; i ++)
		Pour toutes les autres cartes (j=0 ;j<15-i-N ;j++)
			DeckTest = vide ;
			DeckTest.push(DeckPartie[i])
			Pour N + 2 cartes (k=1 ;k<2+N ;k++)
				DeckTest.push(DeckPartie[i+k+j])
			Tab = AssemblageArbre(copie(Decktest) , Decktest[0])
			Pour toutes les valeurs de tab
				Si tab[m] == N+2
				//Woula j’ai trouvé*/
	for (var N = 0; N < 4; N++) {
		for (var i = 0; i < 16 - N; i++) {
			for (var j = 0; j < 15 - i - N; j++) {
				var DeckTeste = [];
				var tabCode = [];
				var tab = [];
				var copie = [];
				DeckTeste.push(deckPartie[i]);
				for (var k = 1; k < N + 2; k++) {
					DeckTeste.push(deckPartie[i + k + j])
				}
				tab = AssemblageARBRE(copieListeDeCarte(DeckTeste), copieCarte(DeckTeste[0]));

				//Recupération de code

				var solution = true;
				for (var h = 0; h < tab.length; h++) {
					tabCode.push(tab[h].code);
				}
				//Reagarde si il y a une solution
				for (var g = 0; g < tab.length; g++) {
					if (tab[g].code == DeckTeste.length) {
						solution = false;
					}
				}
				//Reagarde si il y a une solution
				if (!solution) {
					Cptsolution++;
					var recup = [];
					for (var h = 0; h < DeckTeste.length; h++) {
						recup.push(deckPartie.indexOf(DeckTeste[h]) + 1);
					}
					if(!modelangue){
						window.alert("Combinaison a " + (N + 2) + " cartes trouvée : " + recup);
					}
					else{
						window.alert("Combination with " + (N + 2) + " cards found : " + recup);
					}

				}
			}
		}
	}
	if(!modelangue){
		window.alert("Nombre de combinaison: " + Cptsolution);
	}
	else{
		window.alert("Number of combination: " + Cptsolution);
	}

}

function testPertinent() {
	//test();
	offreSWISH = true;
	lesPoints -= 5;
	document.getElementById("affPoints").textContent = 'Score : ' + lesPoints;
	var allComb = 0;
	//window.alert("Voici ce que j'ai trouvé, mais voulez vous booster mes recherches ?")
	console.log("===============!!!! BOOST !!!!=============")
	for (var i = 2; i < 6; i++) {
		var tabCheck = [];
		for (var j = 0; j < deckPartie.length - i + 1; j++) {
			var tab = recherchePertinente(i, j, j);
			for (var m = 0; m < tab.length; m++) {
				tabCheck.push(tab[m]);
			}

		}
		//console.log("J'ai tout les ensembles pour " + i + " cartes " + tabCheck.length)
		var tabInte = comparaisonEmpilement(tabCheck, 0);
		//console.log("nouveau tableau avec " + tabInte.length)
		var comb = 0;
		comb = testAllEnsemble(tabInte);
		if (comb > 0 && i >= 3) {
			return;
		}
		allComb += comb;
	}
	console.log("nb combi : " + allComb)
	window.alert("Et voilà ! J'ai finis !")
}

function comparaisonEmpilement(liste, debut) {
	if (debut >= liste.length - 1) {
		return liste;
	}
	var tabPropre = [];
	for (var j = 0; j <= debut; j++) {
		tabPropre.push(liste[j]);
	}
	var sommeCarreEle = sommeCarreElement(liste[debut].ensemble);
	for (var i = debut + 1; i < liste.length; i++) {
		if (sommeCarreEle != sommeCarreElement(liste[i].ensemble)) {
			tabPropre.push(liste[i]);
		} else {
			//console.log("==========DOUBLON==========")
			//console.log(sommeCarreEle + " ===== " + sommeCarreElement(liste[i].ensemble))
		}
	}
	//console.log(tabPropre.length + " ========== " + liste.length)
	var tabRetour = comparaisonEmpilement(tabPropre, debut + 1);
	return tabRetour;
}
function sommeCarreElement(element) {
	var sommeCarre = 0;
	for (var i = 0; i < element.length; i++) {
		sommeCarre += Math.pow(2, element[i]);
	}
	return sommeCarre;
}

function selectionAuto(liste) {
	while (carteselect.length != 0) {
		var id = "card" + carteselect[0].identifiant;
		document.getElementById(id).style.boxShadow = "";
		carteselect.splice(0, 1);
	}
	for (var i = 0; i < liste.length; i++) {
		var id = "card" + deckPartie[liste[i]].identifiant;
		document.getElementById(id).style.boxShadow = "0 0 1vw red, 0 0 1vw red";
		carteselect.push(deckPartie[liste[i]]);
	}
}

function testAllEnsemble(liste) {
	var tabCode = [];
	var compteur = 0;
	for (var i = 0; i < liste.length; i++) {
		var lesCartes = [];
		for (var j = 0; j < liste[i].ensemble.length; j++) {
			lesCartes.push(deckPartie[liste[i].ensemble[j]]);
		}
		var tab = AssemblageARBRE(copieListeDeCarte(lesCartes), copieCarte(lesCartes[0]));
		//Recupération de code
		var solution = true;
		for (var h = 0; h < tab.length; h++) {
			tabCode.push(tab[h].code);
		}
		//Reagarde si il y a une solution
		for (var g = 0; g < tab.length; g++) {
			if (tab[g].code == lesCartes.length) {
				solution = false;
			}
		}
		//Reagarde si il y a une solution
		if (!solution) {
			selectionAuto(liste[i].ensemble);
			//console.log("==========================")
			if(!modelangue){
				console.log("Combinaison à " + (lesCartes.length) + " cartes trouvée : " + liste[i].ensemble);
			}
			else {
				console.log("Combination with " + (lesCartes.length) + " cards found : " + liste[i].ensemble);
			}

			compteur++;
			//console.log(sommeCarreElement(liste[i].ensemble))
		}
	}
	return compteur;
}

class Ensemble {
	constructor(valeur, index) {
		this.v = valeur;
		this.ensemble = [];
		this.ensemble.push(index);
	}
}

function recherchePertinente(nbCarte, debut, carte) {
	tabRetour = [];
	nbCarte = nbCarte - 1;

	//Si il n'y a  plus de carte à ajouter dans l'ensemble on regarde si la boucle de peut se boucler
	if (nbCarte == 0) {
		var laPremiereCarte = deckPartie[debut];
		for (var i = 0; i < deckPartie[carte].SesFigures.length; i++) {
			for (var j = 0; j < laPremiereCarte.SesFigures.length; j++) {
				if (deckPartie[carte].SesFigures[i].type[0] == TypeFigure.Petit && laPremiereCarte.SesFigures[j].type[0] == TypeFigure.Moyen) {
					if (deckPartie[carte].SesFigures[i].couleur == laPremiereCarte.SesFigures[j].couleur) {
						tabRetour.push(new Ensemble(0, carte));
						return tabRetour;
					} else {
						tabRetour.push(new Ensemble(-1, carte));
						return tabRetour;
					}
				}
			}
		}
	}

	//On recupère tout les index des cartes pouvant compléter le point de la carte à l'index "carte"
	for (var i = 0; i < deckPartie[carte].SesFigures.length; i++) {
		if (deckPartie[carte].SesFigures[i].type[0] == TypeFigure.Petit) {
			var lePoint = deckPartie[carte].SesFigures[i];
		}
	}
	var listeCarteComp = carteComplementaire(lePoint, debut);
	//Si la liste de carte complémentaire est vide alors on retourne un code d'erreur
	if (listeCarteComp.length == 0) {
		tabRetour.push(new Ensemble(-1, carte));
		return tabRetour;
	}

	//Sinon pour chaque index de carte complémentaire on continue la recherche pertinente
	var tabPropre = [];
	for (var i = 0; i < listeCarteComp.length; i++) {
		var tab = [];
		var unTabEnsemble = [];
		tab.push(recherchePertinente(nbCarte, debut, listeCarteComp[i]));

		for (var k = 0; k < tab[0].length; k++) {
			unTabEnsemble.push(tab[0][k]);
		}

		for (var j = 0; j < unTabEnsemble.length; j++) {
			if (unTabEnsemble[j].v == 0) {
				var present = false;
				for (var m = 0; m < unTabEnsemble[j].ensemble.length; m++) {
					if (unTabEnsemble[j].ensemble[m] == carte) {
						present = true;
					}
				}
				if (!present) {
					unTabEnsemble[j].ensemble.push(carte);
					tabPropre.push(unTabEnsemble[j]);
				}
			}
		}

	}

	return tabPropre;



}

function carteComplementaire(figure, debut) {
	var retour = [];
	//Pour toutes les cartes de deckpartie après la carte de début, on regarde les ronds des cartes et on regarde leurs couleurs pour les faire remonter.
	for (var i = debut + 1; i < deckPartie.length; i++) {
		for (var j = 0; j < deckPartie[i].SesFigures.length; j++) {
			if (deckPartie[i].SesFigures[j].type[0] == TypeFigure.Moyen) {
				if (deckPartie[i].SesFigures[j].couleur == figure.couleur) {
					retour.push(i);
				}
			}
		}
	}
	return retour;
}

function test() {
	printCombinations([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 12);
}

function printCombinations(array, p) {
	var combinations = [];
	var Cptsolution = 0;
	for (k = 2; k < p + 1; k++) {

		function run(level, start) {
			for (var i = start; i < array.length - k + level + 1; i++) {
				combinations[level] = array[i];
				//console.log(i);
				if (level < k - 1) {

					run(level + 1, i + 1);
				} else {

					var combTest = [];
					for (var j = 0; j < combinations.length; j++) {
						combTest.push(deckPartie[combinations[j]]);
						//window.alert("Matrice ajouté num" + j + " :" + deckPartie[combinations[j]].getMatrice);
						console.log("i" + j + " :" + combinations[j]);
					}
					console.log("matrice début ex: " + combTest[0].getMatrice);
					console.log("Cartes selectionnées :(en partant de indice 0) :")
					console.log(combinations.join(" "));
					console.log("----- taille :" + combTest.length + " -------");

					var cpt = testCombinaison(combTest, combinations, Cptsolution);
					Cptsolution = cpt;
				}
			}
		}
		run(0, 0);

		if(!modelangue){
			window.alert("fin test " + k + " cartes")
		}
		else{
			window.alert("end test " + k + " cards")
		}


	}
	window.alert("nb combi :" + Cptsolution);
}


function testCombinaison(DeckTeste, combi, Cptsolution) {
	var tabCode = [];
	var tab = [];
	//window.alert("---------------");
	tab = AssemblageARBRE(copieListeDeCarte(DeckTeste), copieCarte(DeckTeste[0]));

	//Recupération de code
	var solution = true;
	for (var h = 0; h < tab.length; h++) {
		tabCode.push(tab[h].code);
	}
	//Reagarde si il y a une solution
	for (var g = 0; g < tab.length; g++) {
		if (tab[g].code == DeckTeste.length) {
			solution = false;
		}
	}
	//Reagarde si il y a une solution
	if (!solution) {
		Cptsolution++;
		if(!modelangue){
			window.alert("Combinaison à " + (DeckTeste.length) + " cartes trouvée : " + combi);
		}
		else{
			window.alert("Combination with " + (DeckTeste.length) + " cards found : " + combi);
		}

	}
	return Cptsolution;
}

function chercheCombi2() {

	var Cptsolution = 0;
	for (var i = 0; i < 15; i++) {

		for (var j = 0; j < 15 - i; j++) {

			var DeckTeste = [];
			var tabCode = [];
			var tab = [];
			var copie = [];
			copie.splice(0, DeckTeste.length);
			DeckTeste.splice(0, DeckTeste.length);
			tab.splice(0, DeckTeste.length);
			tabCode.splice(0, DeckTeste.length);

			DeckTeste.push(copieCarte(deckPartie[i]));
			DeckTeste.push(copieCarte(deckPartie[j + i + 1]));


			for (var h = 0; h < DeckTeste.length; h++) {
				copie.push(DeckTeste[i]);
			}

			tab = AssemblageARBRE(copieListeDeCarte(DeckTeste), copieCarte(DeckTeste[0]));

			var solution = true;


			for (var h = 0; h < tab.length; h++) {
				tabCode.push(tab[h].code);
			}

			for (var g = 0; g < tab.length; g++) {
				if (tab[g].code == DeckTeste.length) {
					solution = false;
				}
			}
			if (!solution) {
				Cptsolution++;
				if(!modelangue){
					window.alert("Combinaison a 2 trouve: " + (i + 1) + " " + (i + j + 2));
				}
				else{
					window.alert("Combination with 2 found: " + (i + 1) + " " + (i + j + 2));
				}

			}
		}
	}
	if(!modelangue){
		window.alert("Nombre de solution a 2 cartes : " + Cptsolution);
	}
	else{
		window.alert("Number of solutions with 2 cards : " + Cptsolution);
	}

}

//=================================================================================
//chercheCombi => Compte les combinainsons de 3 cartes a l'ecran
function chercheCombi3() {
	var Cptsolution = 0;
	for (var i = 0; i < 15; i++) { //Partie fixe

		for (var j = 0; j < 15 - i; j++) {  //Partie movible 1

			for (var k = 0; k < 15 - j; k++) { //Partie movible 2


				var DeckTeste = [];
				var tabCode = [];
				var tab = [];
				var copie = [];
				copie.splice(0, DeckTeste.length);
				DeckTeste.splice(0, DeckTeste.length);
				tab.splice(0, DeckTeste.length);
				tabCode.splice(0, DeckTeste.length);

				DeckTeste.push(deckPartie[i]);
				DeckTeste.push(deckPartie[j + i + 1]);
				DeckTeste.push(deckPartie[k + j + i + 2]);

				for (var h = 0; h < DeckTeste.length; h++) {
					copie.push(DeckTeste[i]);
				}

				for (var z = 0; z < DeckTeste; z++) {
					if (DeckTeste[z] == null) {
						window.alert("Pb carte null :" + i + j + k);
					}
				}

				tab = AssemblageARBRE(copieListeDeCarte(DeckTeste), copieCarte(DeckTeste[0]));
				var solution = true;

				for (var h = 0; h < tab.length; h++) {
					tabCode.push(tab[h].code);
				}



				var DeckTeste = [];
				var tabCode = [];
				var tab = [];
				var copie = [];
				copie.splice(0, DeckTeste.length);
				DeckTeste.splice(0, DeckTeste.length);
				tab.splice(0, DeckTeste.length);
				tabCode.splice(0, DeckTeste.length);

				DeckTeste.push(deckPartie[i]);
				DeckTeste.push(deckPartie[j + i + 1]);
				DeckTeste.push(deckPartie[k + j + i + 2]);

				for (var h = 0; h < DeckTeste.length; h++) {
					copie.push(DeckTeste[i]);
				}

				for (var z = 0; z < DeckTeste; z++) {
					if (DeckTeste[z] == null) {
						window.alert("Pb carte null :" + i + j + k);
					}
				}

				tab = AssemblageARBRE(copieListeDeCarte(DeckTeste), copieCarte(DeckTeste[0]));
				var solution = true;

				for (var h = 0; h < tab.length; h++) {
					tabCode.push(tab[h].code);
				}


				for (var g = 0; g < tab.length; g++) {
					if (tab[g].code == DeckTeste.length) {
						solution = false;
					}
				}
				if (!solution) {
					Cptsolution++;
					if(!modelangue){
						window.alert("Combinaison a 3 trouve: " + (i + 1) + " " + (i + j + 2) + " " + (k + j + i + 3));
					}
					else{
						window.alert("Combination with 3 found: " + (i + 1) + " " + (i + j + 2) + " " + (k + j + i + 3));
					}

				}
				goRefresh();
			}
		}
	}
	if (Cptsolution == 0) {
		if(!modelangue){
			window.alert("Pas de solution");
		}
		else{
			window.alert("No solution");
		}

	}
	if(!modelangue){
		window.alert("Nombre de solution a 3 cartes : " + Cptsolution);
	}
	else{
		window.alert("Number of solutions with 3 cards : " + Cptsolution);
	}

}
//=============================================================================================
//Fonction pour refresh l'affichage

var compteurRefresh = 50;
var refreshJ;

function goRefresh() {
	compteurRefresh = 50;
	refresh();
}

function refresh() {
	//console.log(compteurRefresh)
	if (compteurRefresh > 0) {
		compteurRefresh--;
		refreshJ = setTimeout("refresh()", 100);
	}

	else {
		clearTimeout(refreshJ);
	}

	if (compteurRefresh == 0) {
		document.getElementById("affSolution").textContent = "";
	}
}

//=====================================================================
//Fonctions permettant d'aider le joueur au bout d'un certain temps


var compteurAide = 900;

function rechercheAide() {
	var allComb = 0;
	//window.alert("Voici ce que j'ai trouvé, mais voulez vous booster mes recherches ?")
	console.log("===============!!!! BOOST !!!!=============")
	for (var i = 2; i < 6; i++) {
		var tabCheck = [];
		for (var j = 0; j < deckPartie.length - i + 1; j++) {
			var tab = recherchePertinente(i, j, j);
			for (var m = 0; m < tab.length; m++) {
				tabCheck.push(tab[m]);
			}

		}
		//console.log("J'ai tout les ensembles pour " + i + " cartes " + tabCheck.length)
		var tabInte = comparaisonEmpilement(tabCheck, 0);
		//console.log("nouveau tableau avec " + tabInte.length)
		var comb = 0;
		comb = testAllEnsemble(tabInte);
		if (comb > 0) {
			return;
		}
		allComb += comb;
	}
	if (deckPartie.length < 16 && comb == 0) {
		if (!modelangue) {
			window.alert("Fin de partie !");
			document.getElementById("HighscorePage").style.display = "block";
		} else {
			window.alert("Game Over !");
			document.getElementById("HighscorePage").style.display = "block";
			
		}

		clearInterval(itv);
	} else if (deckPartie.length >= 16 && comb == 0) {
		lesPoints += 5;
		redistribuerPlateau();
	}
	console.log("nb combi : " + allComb)
}

var aideJ;

function goAide() {
	compteurAide = 900;
	aide();
}

function aide() {
	if (compteurAide > 0) {
		compteurAide--;
		aideJ = setTimeout("aide()", 100);
	}

	else {
		clearTimeout(aideJ);
	}

	if (compteurAide == 0) {
		rechercheAide();
	}
}



//=================================================================================
//FONCTION DE TEST => permet de tester des fonctionnalite via le bouton VALIDER


var cpt = 1100;
var x;
var itv = 0;

function decompte() {

	//var progressnum = document.getElementById("progressnum");
	var indicateur = document.getElementById("indicateur");

	if (cpt >= 0) {

		cpt--;
		x = setTimeout("decompte()", 100);
	}

	else {
		clearTimeout(x);
	}

	if (cpt == 0) {
		if(!modelangue){
			window.alert("fin du jeu");
			document.getElementById("HighscorePage").style.display = "block";
		}
		else{
			window.alert("Game Over");
			document.getElementById("HighscorePage").style.display = "block";
		}
		clearInterval(itv);
	}

	if (cpt < 550) {
		document.getElementById("indicateur").style.backgroundColor = "orange";

	}

	if (cpt < 366) {
		document.getElementById("indicateur").style.backgroundColor = "red";
	}

	indicateur.style.width = cpt + "px";
	//progressnum.innerHTML = cpt;
}

function testerLesCartes() {

	if (selectionMode == 0) {
		goAide();
		testPourJeuClassique();
	} else if (selectionMode == 1 || selectionMode==2) {
		goAide();
		testPourJeuInfini();
	}
	if( selectionMode == 3){
		testerCartesMulti();
	}
}


function testPourJeuInfini() {
	try {
		if (carteselect.length == 0) {
			if(!modelangue){
				window.alert("Selection Vide");
			}
			else{
				window.alert("Empty selection");
			}

			return;
		} else if (carteselect.length < 2) {
			if(!modelangue){
				window.alert("Selection Trop Petite");
			}
			else{
				window.alert("Selection Too Small");
			}

			return;
		} else {
			var copie = [];
			for (var i = 0; i < carteselect.length; i++) {
				copie.push(copieCarte(carteselect[i]));
			}
			var tab = [];
			tab = AssemblageARBRE(copieListeDeCarte(copie), copieCarte(carteselect[0]));
			var solution = true;
			var tabCode = [];
			for (var i = 0; i < tab.length; i++) {
				tabCode.push(tab[i].code);
			}
			for (var i = 0; i < tab.length; i++) {
				if (tab[i].code == carteselect.length) {
					solution = false;
					if (offreSWISH) {
						offreSWISH = false;
					} else {
						lesPoints += carteselect.length;
					}
					document.getElementById("affPoints").textContent = 'Score : ' + lesPoints;
					if (selectionDiemension == 2) {
						changerlesCartes3_2();
					} else if (selectionDiemension == 3) {
						changerlesCartes2_2();
					} else {
						changerlesCartes();
					}
				}
			}
			if (solution) {
				if(!modelangue){
					document.getElementById("affSolution").textContent = 'Aucune solution trouvée...';
				}
				else{
					document.getElementById("affSolution").textContent = 'No solutions found...';
				}

			}
			if (!solution) {
				if(!modelangue){
					document.getElementById("affSolution").textContent = 'Assemblage de ' + copie.length + ' cartes trouvé !';
				}
				else{
					document.getElementById("affSolution").textContent = 'Assembly of ' + copie.length + ' cards found !';
				}

			}
			goRefresh();
		}
	} catch (e) {
		window.alert(e);
	}
}

//Fonction de test mais pour le jeu classique

function testPourJeuClassique() {
	//try {
	if (carteselect.length == 0) {
		if(!modelangue){
			window.alert("Selection Vide test pour jeu classique");
		}
		else{
			window.alert("Selection Empty test for classic game");
		}

		return;
	} else if (carteselect.length < 2) {
		if(!modelangue){
			window.alert("Selection Trop Petite");
		}
		else{
			window.alert("Selection Too Small");
		}

		return;
	} else {
		var copie = [];
		for (var i = 0; i < carteselect.length; i++) {
			copie.push(copieCarte(carteselect[i]));
		}
		var tab = [];
		tab = AssemblageARBRE(copie, copieCarte(carteselect[0]));
		var solution = true;
		var tabCode = [];
		for (var i = 0; i < tab.length; i++) {
			tabCode.push(tab[i].code);
		}
		for (var i = 0; i < tab.length; i++) {
			if (tab[i].code == carteselect.length) {
				solution = false;
				cpt = cpt + 20 * copie.length;
				if (offreSWISH) {
					offreSWISH = false;
				} else {
					lesPoints += carteselect.length;
				}
				document.getElementById("affPoints").textContent = 'Score : ' + lesPoints;
				changerlesCartesDeTasDeJeu();
			}
		}
		if (solution) {
			if(!modelangue){
				document.getElementById("affSolution").textContent = 'Aucune solution trouvée...';
			}
			else{
				document.getElementById("affSolution").textContent = 'No solutions found...';
			}

		}
		if (!solution) {
			if(!modelangue){
				document.getElementById("affSolution").textContent = 'Assemblage de ' + copie.length + ' cartes trouvé ! Il reste encore ' + TasDuJEU.length + ' cartes !';
			}
			else{
				document.getElementById("affSolution").textContent = 'Assembly of ' + copie.length + ' cards found ! There are still ' + TasDuJEU.length + ' cards !';
			}
		}
		goRefresh();
	}
	/*} catch (e) {
		window.alert(e);
	}*/
}

//===================================================================================
//Function de copie diverse

function copieListeDeCarte(Liste) {
	var copie = [];
	for (var i = 0; i < Liste.length; i++) {
		copie.push(copieCarte(Liste[i]));
	}
	return copie;
}

function copieCarte(uneCarte) {
	if (uneCarte == null) {
		return null;
	}
	var AllFigure = [];
	for (var i = 0; i < uneCarte.SesFigures.length; i++) {
		AllFigure.push(new Figure(uneCarte.SesFigures[i].type, uneCarte.SesFigures[i].forme, uneCarte.SesFigures[i].X, uneCarte.SesFigures[i].Y, uneCarte.SesFigures[i].couleur));
	}
	var carte = new Carte(uneCarte.getIdentifiant, AllFigure, uneCarte.row, uneCarte.column);
	carte.Matrice = copieMatrice(uneCarte.Matrice, uneCarte.row, uneCarte.column);
	return carte;
}

function copieLiseDeFigure(Liste) {
	var copie = [];
	for (var i = 0; i < Liste.length; i++) {
		copie.push(copieFigure(Liste[i]));
		window.alert("code " + copie[i].CodeHTML)
	}
	return copie;
}

function copieFigure(uneFigure) {
	if(!modelangue){
		window.alert("Avant copiage " + uneFigure.CodeHTML)
	}
	else{
		window.alert("Before copying " + uneFigure.CodeHTML)
	}

	if (uneFigure == null) {
		return null;
	}
	if(!modelangue){
		window.alert("je fais la copie");
	}
	else{
		window.alert("I make the copy");
	}

	var copie = new Figure(uneFigure.type, uneFigure.forme, uneFigure.X, uneFigure.Y);
	return copie;
}
//=====================================================================================================
//Function de manipulataion de Carte

function Verticale(coCarte) {
	var laCarte;
	laCarte = copieCarte(coCarte);
	laCarte = laCarte.carteVerticale;
	return laCarte;
}

function Horizontale(coCarte) {
	var laCarte;
	laCarte = copieCarte(coCarte);
	laCarte = laCarte.carteHorizontale;
	return laCarte;
}

//==============================================================================================================================

//FONCTION POUR TESTER L'ARBRE

class CODE {
	constructor(Carte) {
		this.code = 0;
		this.laCarte = Carte;
	}
}

function AssemblageARBRE(TasDeCarte, Carte) {
	var uneTable = copieListeDeCarte(TasDeCarte);
	var tab = new Array();
	var tabCode = new Array();
	uneTable.splice(0, 1);
	if (uneTable.length == 0) {
		var leCode = new CODE(Carte);
		tab.push(leCode);
		return tab;
	}
	tab.push(AssemblageARBRE(uneTable, copieCarte(uneTable[0])));
	tab.push(AssemblageARBRE(uneTable, Horizontale(copieCarte(uneTable[0]))));
	tab.push(AssemblageARBRE(uneTable, Verticale(copieCarte(uneTable[0]))));
	var derniereCarte = Horizontale(Verticale(copieCarte(uneTable[0])));
	tab.push(AssemblageARBRE(uneTable, derniereCarte));
	for (var i = 0; i < tab.length; i++) {
		for (var j = 0; j < tab[i].length; j++) {
			tabCode.push(tab[i][j]);
		}
	}

	for (var i = 0; i < tabCode.length; i++) {
		if (tabCode[i].code != -1) {
			var enregistrement = tabCode[i].code;
			tabCode[i] = comparaisonARBRE(tabCode[i].laCarte, Carte);
			tabCode[i].code += enregistrement;
		}
	}
	return tabCode;
}


function comparaisonARBRE(CarteMereEntree, CarteFilleEntree) {
	var retour = [];
	var liaison = 0;
	var CarteMere = copieCarte(CarteMereEntree);
	var CarteFille = copieCarte(CarteFilleEntree);
	for (var i = 0; i < CarteFille.SesFigures.length; i++) {
		for (var j = 0; j < CarteMere.SesFigures.length; j++) {
			if (CarteFille.SesFigures[i].X == CarteMere.SesFigures[j].X) {
				if (CarteFille.SesFigures[i].Y == CarteMere.SesFigures[j].Y) {
					//Coordonnees identiques
					if (CarteFille.SesFigures[i].forme == CarteMere.SesFigures[j].forme) {
						//Forme identiques
						for (var k = 0; k < CarteMere.SesFigures[j].type.length; k++) {
							if (CarteFille.SesFigures[i].couleur != CarteMere.SesFigures[j].couleur) {//Si les figures ne sont pas de la même couleur on stop la comparaison
								var leCode = new CODE(CarteMere);
								leCode.code = -1;
								return leCode;
							}
							if (CarteFille.SesFigures[i].type[0] == TypeFigure.Petit) {
								if (CarteMere.SesFigures[j].type[k] == TypeFigure.Petit) {
									var leCode = new CODE(CarteMere);
									leCode.code = -1;
									return leCode;
								}
							} else if (CarteFille.SesFigures[i].type[0] == TypeFigure.Moyen) {
								if (CarteMere.SesFigures[j].type[k] == TypeFigure.Moyen) {
									var leCode = new CODE(CarteMere);
									leCode.code = -1;
									return leCode;
								}
							} else if (CarteFille.SesFigures[i].type[0] == TypeFigure.Grand) {
								if (CarteMere.SesFigures[j].type[k] == TypeFigure.Grand) {
									var leCode = new CODE(CarteMere);
									leCode.code = -1;
									return leCode;
								}
							}
						}
						//Si le type de la figure n'y est pas on l'ajoute a la liste de carte Mere;
						liaison = liaison + 1;
						CarteMere.SesFigures[j].type.push(CarteFille.SesFigures[i].type[0]);
					} else {
						var leCode = new CODE(CarteMere);
						leCode.code = -1;
						return leCode;
					}
				}
			}
		}
	}
	var CarteSomme = SommeDeCarte(CarteMere, copieCarte(CarteFille));
	var leCode = new CODE(CarteSomme);
	leCode.code = liaison;
	return leCode;
}

function SommeDeCarte(CarteMere, CarteFille) {
	CarteMere.Matrice = creaMatriceVierge(CarteMere.row, CarteMere.column);
	for (var i = 0; i < CarteFille.SesFigures.length; i++) {
		var manquant = true;
		for (var j = 0; j < CarteMere.SesFigures.length; j++) {
			if (CarteFille.SesFigures[i].X == CarteMere.SesFigures[j].X) {
				if (CarteFille.SesFigures[i].Y == CarteMere.SesFigures[j].Y) {
					manquant = false;
				}
			}
		}
		if (manquant) {
			CarteMere.SesFigures.push(CarteFille.SesFigures[i]);
		}
	}

	for (var i = 0; i < CarteMere.SesFigures.length; i++) {
		for (var j = 0; j < CarteMere.SesFigures[i].type.length; j++) {
			if (CarteMere.SesFigures[i].type[j] == TypeFigure.Petit) {
				CarteMere.Matrice[CarteMere.SesFigures[i].X][CarteMere.SesFigures[i].Y] += 1;
			} else if (CarteMere.SesFigures[i].type[j] == TypeFigure.Moyen) {
				CarteMere.Matrice[CarteMere.SesFigures[i].X][CarteMere.SesFigures[i].Y] += 2;
			} else if (CarteMere.SesFigures[i].type[j] == TypeFigure.Grand) {
				CarteMere.Matrice[CarteMere.SesFigures[i].X][CarteMere.SesFigures[i].Y] += 4;
			}
		}
	}
	return CarteMere;
}

//================================================================================================================================

//==================================================================================




function doublonInterdit(uneCarte) {
	var lesCoPetit = [];
	lesCoPetit.push([1, 1], [1, 1], [1, 2], [1, 2], [1, 1], [2, 2], [1, 1], [2, 1], [1, 2], [2, 2], [1, 2], [2, 1]);
	var lesCoMoyens = [];
	lesCoMoyens.push([2, 1], [3, 3], [1, 1], [3, 4], [2, 3], [1, 4], [1, 2], [1, 1], [2, 3], [1, 3], [2, 4], [1, 3]);
	for (var i = 0; i < lesCoPetit.length; i++) {
		if (uneCarte.SesFigures[0].X == (lesCoPetit[i][0]) - 1) {
			if (uneCarte.SesFigures[0].Y == (lesCoPetit[i][1]) - 1) {
				if (uneCarte.SesFigures[1].X == (lesCoMoyens[i][0]) - 1) {
					if (uneCarte.SesFigures[1].Y == (lesCoMoyens[i][1]) - 1) {
						return true;
					}
				}
			}
		}
	}
	return false;
}

function bacASable() {
	majSizeCard(3);
	var lesCartes = [];

	var LesFigure = [];
	var uneFigure = new Figure(TypeFigure.Petit, FormeFigure.Rond, 0, 0, CouleurFigure.Couleur1);
	LesFigure.push(uneFigure);
	var uneFigure = new Figure(TypeFigure.Petit, FormeFigure.Rond, 1, 0, CouleurFigure.Couleur2);
	LesFigure.push(uneFigure);
	var uneCarte = new Carte(1, LesFigure, 3, 2);

	lesCartes.push(uneCarte);

	afficherCartes(lesCartes);
}



function functionLange(){
	//window.alert(modelangue);
	console.log(modelangue);
	if(modelangue){
		modelangue = false;
		//MODE FRANCAIS =======
		document.getElementById('flag_top').src = "imgs/flags/flag-france.png";
		document.getElementById('flag_top2').src = "imgs/flags/flag-france.png";

		document.getElementById('textjouerr').innerHTML = 'JOUER';

		//Traduction classement
		document.getElementById('classementtxtTimer').textContent = 'Classement - Timer';
		document.getElementById('classementtxtMatch').textContent = 'Classement - Match à Point';

		//Traduction paramètres
		document.getElementById('SettingName').textContent = 'Paramètres';

		//Tradcution des règles
		document.getElementById('lesRegles').textContent = 'Les Règles';
		document.getElementById('textRules').textContent = 'Swish : Jeu de 60 cartes composé chacun de billes et de cerceaux, de 4 couleurs différentes possibles. 16 cartes sont posées sur une table et le joueur doit faire superposer des cartes de manière que chaque bille soit dans le cerceau de même couleur d’une autre carte.';

		document.getElementById("textmode").textContent = "" + allModeFrench[selectionMode]


		document.getElementById('textmulticreer').textContent = 'Créer';
		document.getElementById('textmultirejoindre').textContent = 'Rejoindre';

		document.getElementById('nominputHS').textContent = 'Nom :';

		document.getElementById('idinput_multi').placeholder = 'ID Partie';
	}

	else{
		modelangue = true;
		//MODE ANGLAIS ========
		document.getElementById('flag_top').src = "imgs/flags/flag-uk.png";
		document.getElementById('flag_top2').src = "imgs/flags/flag-uk.png";

		document.getElementById('textjouerr').innerHTML = 'PLAY';

		//Traduction classement
		document.getElementById('classementtxtTimer').textContent = 'Ranking - Timer';
		document.getElementById('classementtxtMatch').textContent = 'Ranking - Point Match';

		//Traduction paramètres
		document.getElementById('SettingName').textContent = 'Settings';



		//Traduction des règles
		document.getElementById('lesRegles').textContent = 'The Rules';
		document.getElementById('textRules').textContent = 'Swish : Set of 60 cards each composed of balls and hoops, 4 different colors possible. 16 cards are placed on a table and the player has to lay cards on top of each other in such a way that each ball is in the same color hoop as another card.'

		document.getElementById("textmode").textContent = "" + allModeEngl[selectionMode]

		document.getElementById('textmulticreer').textContent = 'Create';
		document.getElementById('textmultirejoindre').textContent = 'Join';

		document.getElementById('nominputHS').textContent = 'Name :';

		document.getElementById('idinput_multi').placeholder = 'ID Game';

	}
}

 function recuphighscore(){
	var nom = document.getElementById('nom').value;
	var score = lesPoints;
	$.ajax({
		type: "POST",
		url: "ajouterScore.php",
		data: {
			score : score,
			nom: nom,
			modeJeu : selectionMode
		},
		success: function(data){
			if(data == 1)
				alert("requête effectuée ");
			else
				alert("retapez votre nom svp");
		}
	});

 }

 function reloadAfterHS(){
	retour();
	window.location.reload();

 }

