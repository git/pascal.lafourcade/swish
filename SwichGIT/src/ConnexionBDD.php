<?php
class MyDB extends SQLite3
{
    function __construct()
    {
        $this->open('mysqlitedb.db');
        //$this->exec('CREATE TABLE partieScore(IdPartie STRING PRIMARY KEY,scorePremierJ STRING,scoreDeuxiemeJ STRING, pseudoPremierJ STRING,pseudoDeuxiemeJ STRING)');
        //$this->exec('CREATE TABLE partieCartes(IdPartie STRING ,IdCarte STRING,colUn STRING, colDeux STRING, colTrois STRING)');

    }
}

function genererChaineAleatoire($longueur = 10)
{
    $caracteres = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $longueurMax = strlen($caracteres);
    $chaineAleatoire = '';
    for ($i = 0; $i < $longueur; $i++)
    {
        $chaineAleatoire .= $caracteres[rand(0, $longueurMax - 1)];
    }
    return $chaineAleatoire;
}

